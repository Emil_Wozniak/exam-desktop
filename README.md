# Examination App

[![Build Status](https://jenkins.platform.dev.nuxeo.com/buildStatus/icon?job=nuxeo/nuxeo/master)](https://jenkins.platform.dev.nuxeo.com/job/nuxeo/job/nuxeo/job/master/)

[[_TOC_]]

## About the Examination App

It's open source project to process over RIA results file produced by MicroBeta2.

### Why Examination App?

The reason to create this app was to prevent manually copying data from files created by the laboratory counter.
[Example](./assets/samples/A16_244.txt) of this file can be found in the project's files as well as excel formula.

Application solves multiple problems:

- will find appropriate metadata in the file, for example column **CCPM1**;
- will read those metadata and performs calculations, like those in [XLSX](./assets/samples/kortyzol%20%20A16_244.xlsx);
- will display all results as a table in the web browser;
- will allow you to customize [Standard Curve](https://en.wikipedia.org/wiki/Standard_curve);
- will display **Standard Curve** as a picture in the web browser;

## Installation

Running the Examination App requires JDK 11, Oracle's JDK or OpenJDK and JavaFX in the classpath.

### JavaFX instruction

You need to have **.javafx** directory with **lib** and **bin** JavaFX dirs in your home directory.

- Download JavaFX 11 [from here](https://gluonhq.com/products/javafx/)
- Extract zip in you download directory
- Rename JavaFX extracted directory to **.javafx**
- Move **.javafx** to your home directory

### Requirements

- Java 11
- Kotlin 1.4.21
- Gradle >= 6.7
- JavaFX 11.02

## Distribution

After create zip file go to [distributions](./build/distributions/) unzip the file and find 
**--module-path=/home/emil/.javafx/lib** you must change */home/emil/* into your environment variables for each OS 
`$HOME\` for Unix and `%HOMEPATH%\` for Windows. Before you zip it copy [install guide](./assets/install.txt).

## Use

Download last release file [Release](https://gitlab.com/Emil_Wozniak/exam-desktop/-/releases) and unzip, inside unzip
directory you will find **bin** directory where executable files are located. You may need to allow this application to
run in that case:

**Linux**
```bash
unzip exam.zip
chmod +x exam/bin/exam
# run
./exam/bin/exam
```

**Windows**
```
unzip exam.zip
go to exam/bin
right-click exam.bat > properties > security
Edit permissions
```


## Building

Building the Examination App requires the following tools:

- Git (obviously)
- Kotlin 1.4.2
- JDK 11 (OpenJDK recommended)
- Openjfx 11
- Gradle 6.8.2 (7.0-rc-2 recommended)
- Sdkman

Get the source code:

```shell
git clone git@gitlab.com:Emil_Wozniak/exam-desktop.git
cd exam-desktop
```

To build everything, including the packages, ZIP, TAR, run:

```shell
./gradlew assembleDist
```

To run distribution app, run:

**Linux**

```shell
unzip build/distributions/exam.zip -d build/distributions
build/distributions/exam/bin/exam
```

**Windows**

```shell
unzip build/distributions/exam.zip -d build/distributions
build/distributions/exam/bin/exam.bat
```

## Test configuration

[IntelliJ Gradle Configuration](https://www.jetbrains.com/help/idea/gradle.html#gradle_vm_options)
In the Settings/Preferences dialog `Ctrl+Alt+S` , go to *Build, Execution, Deployment* | *Gradle*

In a section *Run tests using* select **IntelliJ IDEA**

## Resources

### Documentation

The documentation for the Examination App is not available yet.

### Benchmarks

The Examination App is not available yet.

### Reporting Issues

You can report issues on [Examination App](https://gitlab.com/Emil_Wozniak/exam-desktop/-/issues).

## Licensing

Most of the source code in the Examination App is copyright Emil Woźniak, and licensed under the Apache License, Version
2.0.

See the [LICENSE](LICENSE).

## About Me

I have been a chemical process analyst at The Kielanowski Institute of Animal Physiology and Nutrition Polish Academy of
Sciences. My work consisted, among other things, of recalculating units and examining analysis results. During this time
I started to learn programming languages in order to improve repetitive analysis processes. After about a year of
learning the basics of Java and JavaScript, in December 2018 I acquired the course "Full Stack. Project: Spring Boot
2.0, ReactJS, Redux" on the basis of which I started writing the application two months later.