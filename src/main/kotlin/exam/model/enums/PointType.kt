package exam.model.enums

enum class PointType {
    T, NSB, ZERO, K
}
