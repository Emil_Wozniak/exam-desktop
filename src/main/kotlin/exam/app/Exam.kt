package exam.app

import exam.view.MainView
import exam.view.ScreenSize
import javafx.stage.Stage
import tornadofx.*
import java.net.URI
import java.nio.file.Path
import java.nio.file.Paths

class Exam : App(MainView::class, Styles::class) {
    init {
        addCss("Charts.css")
        addCss("DefaultTheme.css")
        addCss("Fonts.css")
    }

    private val modifier = 0.9
    override fun start(stage: Stage) {
        super.start(stage)
        with(ScreenSize) {
            stage.width = width * modifier
            stage.height = height * modifier
        }
    }

    private fun addCss(name: String) =
        Paths.get("src/main/resources/exam/app/$name")
            .let(Path::toUri)
            .let(URI::toURL)
            .toExternalForm()
            .let(::importStylesheet)
}

fun main(args: Array<String>) {
    launch<Exam>(args)
}