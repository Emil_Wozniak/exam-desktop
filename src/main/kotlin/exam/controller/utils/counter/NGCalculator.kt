package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms
import exam.model.curve.CalibrationCurveValues
import org.apache.commons.math3.util.Precision.round
import kotlin.math.log10

object NGCalculator : Algorithms {

    /**
     * Excel version:
     * = 10^ * (( LOG((B44-$I$16) *100 /$J$18 / (100-(B44-$I$16) *100 /$J$18)) -$R$19) /$R$20)
     *
     *
     * calculates the value of hormone nanograms by formula:
     * { 10^(( LOG((cmp-zero) *100 / binding / (100-(cmp-zero) *100 / binding)) - component.getRegressionParameterA()) / component.getRegressionParameterB())}
     *
     * @return the value of hormone nanograms in the sample
     */
    fun calculate(
        cpms: List<Double>,
        calibrationCurveValues: CalibrationCurveValues,
        parameterA: Double,
        parameterB: Double
    ): List<Double> {
        var wrongs = 0
        return runCatching {
            cpms.asSequence()
                .map { processCPM(it, calibrationCurveValues) }
                .map { log10(it) }.map { it - parameterA }
                .map { it / parameterB }
                .map { powerBy10(it) }
                .map {
                    if (it.isNaN() || it.isInfinite()) {
                        wrongs += 1
                        0.0
                    } else it
                }
                .map { round(it, 2) }
                .toList()
        }
            .onSuccess { println("Count nanogram values finished with:${Ansi.COLOR.GREEN} ${it.size} ${Ansi.RESET}results") }
            .onFailure { println("Count nanogram values failed with:${Ansi.COLOR.GREEN} ${it.cause} ${Ansi.RESET}${Ansi.COLOR.GREEN} ${it.message} ${Ansi.RESET}") }
            .map {
                if (wrongs > 0) {
                    println("Found ∞ or NaN points: ${Ansi.COLOR.YELLOW}[$wrongs]${Ansi.RESET}")
                }
                it
            }
            .getOrElse { cpms }
    }

    private fun processCPM(cpm: Double, calibrationCurveValues: CalibrationCurveValues): Double = cpm.let {
        val (_, nsbs, _, binding) = calibrationCurveValues
        val first = (it - nsbs.toDouble()) * 100 / binding.toDouble()
        val second = 100 - (it - nsbs.toDouble()) * 100 / binding.toDouble()
        first / second
    }
}
