package exam.view

import javafx.scene.layout.BorderPane
import tornadofx.*

class MainView : View("Examination App") {
    private val modifier = 0.9
    override val root = BorderPane().apply {
        with(ScreenSize) {
            prefWidth = width * modifier
            prefHeight = height * modifier
        }
        padding = insets(20, 20, 20, 20)
        addClass("panel")
        this += vbox { label(title) { addClass("h1") } }
        this.right = Sidebar.root
        this.left = TableController.root
    }
}