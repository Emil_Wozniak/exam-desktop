package exam.controller.utils.analizer.extractor

import exam.model.hormone.HormonePattern
import exam.model.results.ExaminationPoint

fun points(
    pattern: HormonePattern,
    length: Int,
    filename: String,
    patternData: String,
    cpms: List<Int>,
    flags: List<Boolean>,
    ng: List<Double>,
    avg: List<Double>
): List<ExaminationPoint> = (pattern.points.size until length)
    .map {
        ExaminationPoint(
            average = avg[it],
            identifier = filename,
            pattern = patternData,
            probeNumber = it,
            position = "$it",
            cpm = cpms[it],
            flagged = flags[it],
            ng = "${ng[it]}"
        )
    }
