package pl.ejdev.analyze.model.standard

import pl.ejdev.analyze.utils.copyEach

object StandardGenerator {
    private const val STANDARD_POINT_AMOUNT = 7
    private const val BASE = 1.250

    fun generateCortizol(): List<Double> {
        var current = 1
        return (0 until STANDARD_POINT_AMOUNT)
            .map {
                if (it == 0) current
                else {
                    current += current
                    current
                }
            }
            .map { it * BASE }
            .copyEach(repeats = 2)
    }
}

data class StandardPoint(
    val std: Double,
    val cpm: Int,
    val bindingPercent: Double,
    val logDose: Double,
    val logarithmRealZero: Double
)