package exam.controller.function

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ObservableList
import javafx.scene.chart.XYChart
import exam.controller.utils.analizer.FileAnalyzer
import exam.model.results.BaseGraph
import exam.model.results.CurvePoint
import exam.model.results.ExaminationPoint
import exam.model.results.ExaminationResults
import exam.model.table.Curve
import exam.model.table.Point
import tornadofx.Controller
import tornadofx.ViewModel
import tornadofx.asObservable
import tornadofx.observableListOf

class ResultPropertyController : Controller() {

    private val analyzer: FileAnalyzer by inject()
    private val controller: exam.controller.TableController by inject()

    val model = ViewModel()
    val curve = model.bind { SimpleListProperty<Curve>() }
    val points = model.bind { SimpleListProperty<Point>() }
    val r = model.bind { SimpleStringProperty("") }
    val zero = model.bind { SimpleStringProperty("") }

    val line1 = mutableListOf<XYChart.Data<Number, Number>>().asObservable()
    val line2 = mutableListOf<XYChart.Data<Number, Number>>().asObservable()

    private val hasChanged = SimpleBooleanProperty(false)

    fun update(point: Curve) {
        println("Update: $point")
        curve
            .filter { it.probeNumber == point.probeNumber }
            .forEach { it.cpm = point.cpm }
        hasChanged.set(true)
    }


    fun setCurve(controlCurve: List<CurvePoint?>) {
        val observableCurve = observableListOf<Curve>()
        controlCurve
            .filterNotNull()
            .map { Curve(it) }
            .forEach { observableCurve.add(it) }
        curve.clear()
        curve.set(observableCurve)
    }

    fun setPoints(resultPoints: List<ExaminationPoint?>) {
        val observablePoints = observableListOf<Point>()
        resultPoints
            .filterNotNull()
            .map { Point(it) }
            .forEach { observablePoints.add(it) }
        points.clear()
        points.set(observablePoints)
    }

    fun setGraph(graph: BaseGraph?) {
        fillCoords(line1, true, graph)
        fillCoords(line2, false, graph)
        r.set("R: ${graph?.r}")
        zero.set("Zero binding: ${graph?.zeroBindingPercent}")
    }

    @Suppress("UNCHECKED_CAST")
    private fun fillCoords(
        coords: ObservableList<XYChart.Data<Number, Number>>,
        even: Boolean,
        graph: BaseGraph?
    ) {
        coords.clear()
        graph!!.coordinates
            .withIndex()
            .filter { it.index % 2 == if (even) 1 else 0 }
            .map { XYChart.Data(it.value.x, it.value.y) }
            .forEach { coords.add(it as XYChart.Data<Number, Number>) }
    }
}
