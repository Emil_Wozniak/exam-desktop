plugins {
    groovy
    alias(libs.plugins.kotlin)
}

val target = libs.versions.jvm.target.get()
tasks.compileKotlin { kotlinOptions.jvmTarget = target }
tasks.compileTestKotlin { kotlinOptions { jvmTarget = target } }

dependencies {
    implementation(libs.kotlin.std)
    implementation(libs.commons.math3)
    implementation(libs.kotlin.logging) { exclude(group = "org.jetbrains.kotlin") }

    implementation(libs.slf4j.jdk14)

    testImplementation(libs.spock.reports) { isTransitive = false}
    testImplementation(libs.junit.jupiter)
    testImplementation(libs.junit.jupiter.api)
    testImplementation(libs.junit.vintage.engine)
    testImplementation(libs.slf4j.api) { isTransitive = false }
    testImplementation(libs.slf4j.simple) { isTransitive = false }
}

tasks.test {
    systemProperty("com.athaydes.spockframework.report.showCodeBlocks", "false")
    useJUnitPlatform()
}
