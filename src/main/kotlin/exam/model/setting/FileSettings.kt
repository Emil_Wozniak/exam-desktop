package exam.model.setting

/**
 * @property target  ` \tUnk` by default, target
 * value represents a string which should precede results in file
 * @property defaults when true, it will cause to create a Cortisol
 * pattern; when false and the rest of property is set properly
 * it will cause a Custom pattern
 * @property pattern is "2;3;3;7;2;0" by default but when
 * defaults property is false should contain six digits
 * separated by semicolons
 * @property startValue "1.25" by default, it should contain
 * string representation of double value
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class FileSettings(
    val defaults: Boolean = true,
    val pattern: String = "2;3;3;7;2;0",
    val startValue: String = "0.39",
    val target: String = " \tUnk"
)
