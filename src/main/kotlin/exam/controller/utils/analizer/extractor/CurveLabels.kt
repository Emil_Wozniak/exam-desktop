package exam.controller.utils.analizer.extractor

import exam.model.hormone.HormonePattern

fun labels(pattern: HormonePattern): List<String> =
    runCatching { pattern.points.map { it.name } }
        .onFailure { println(it.message) }
        .getOrElse { emptyList() }
