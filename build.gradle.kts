plugins {
    application
    idea
    alias(libs.plugins.kotlin)
    alias(libs.plugins.javafxplugin)
    alias(libs.plugins.jlink)
    alias(libs.plugins.moduleplugin)
    alias(libs.plugins.sass)
}

dependencies {
    /*      CORE         */
    implementation(libs.kotlin.std)
    implementation(libs.kotlin.reflect)
    implementation(libs.controlsfx)
    implementation(platform(libs.ikonli.bom))
    implementation(libs.ikonli.javafx)
    implementation(libs.ikonli.materialdesign2.pack)
    implementation(libs.materialfx)
    implementation(libs.tornadofx) { exclude(group = "org.jetbrains.kotlin") }
    implementation(project(":calculator"))

    /*      UTILS        */
    implementation(libs.cyclops)
    implementation(libs.poi)
    implementation(libs.poi.ooxml) { exclude(group = "org.codehaus.stax2") }

    /*      TESTING          */
    testImplementation(libs.kotlin.std)
    testImplementation(libs.kotlin.reflect)
    testImplementation(libs.junit.jupiter)
    testImplementation(libs.kluent)
}

val target = libs.versions.jvm.target.get()
tasks.compileKotlin { kotlinOptions.jvmTarget = target }
tasks.compileTestKotlin { kotlinOptions { jvmTarget = target } }

/* https://github.com/TestFX/TestFX */
tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("exam.app.Exam")
    applicationName = libs.versions.module.name.get()
    mainModule = libs.versions.module.name.get()
}

val delimiter = " "

javafx {
    version = libs.versions.jvm.target.get()
    modules = libs.versions.javafx.modules.get().split(delimiter)
}

jlink {
    options = libs.versions.jlink.options.get().split(delimiter)
    launcher {
        name = "app"
        moduleName = libs.versions.module.name.get()
    }
    forceMerge("kotlin", "tornadofx", "org.hamcrest", "kotest", "java.base", "org.apache.logging.log4j")
    addExtraDependencies("javafx", "poi", "org.apache.poi", "tornadofx", "org.apache.logging.log4j")
    imageZip.set(project.file("${buildDir}/image-zip/app-image.zip"))
}

sass { cssDir = "src/main/resources/exam/app" }

val SASS_COMPILE = "sassCompile"
tasks.named(SASS_COMPILE).get().doLast {
    file("src/main/resources/exam/app").listFiles()?.forEach { css ->
        val lines = css.readLines()
        if (lines.contains("""@charset "UTF-8";""")) {
            println(css.name)
            css.writeText("")
            lines.drop(1).forEach(css::writeText)
        }
    }
}

tasks.named("run").get().dependsOn(SASS_COMPILE)
tasks.named("jlinkZip").get().dependsOn(SASS_COMPILE)
tasks.named("jlink").get().dependsOn(SASS_COMPILE)

tasks.wrapper {
    distributionType = Wrapper.DistributionType.ALL
}

