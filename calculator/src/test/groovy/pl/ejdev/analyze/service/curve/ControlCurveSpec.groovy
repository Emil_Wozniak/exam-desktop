package pl.ejdev.analyze.service.curve

import pl.ejdev.analyze.model.data.DataSettings
import pl.ejdev.analyze.model.data.ProcessedData
import pl.ejdev.analyze.service.BaseSpec
import spock.lang.Subject

class ControlCurveSpec extends BaseSpec {

    private final def processedData = new ProcessedData(lines, DataSettings.default)

    @Subject
    private def analyzer = ControlCurveAnalyzer.INSTANCE

    def "control curve points matches expectation"() {
        when: "analyzer analyse read values"
        def controlCurve = analyzer.analyze(processedData.createCurveReadValues())

        then: "return control curve points matching expected values"
        with(controlCurve) {
            totals == TOTAL
            zeros == ZERO
            nsbs == NSBS
            binding == BINDING
        }
    }

    private final static def TOTAL = 1979
    private final static def ZERO = 34
    private final static def NSBS = 455
    private final static def BINDING = 421

}