package exam.controller.utils.analizer

import exam.model.results.CurvePoint

object ControlCurveCreator {

    fun createPoints(
        size: Int,
        fileName: String,
        patternData: String,
        labels: List<String>,
        cpms: List<Int>,
        flags: List<Boolean>
    ): List<CurvePoint> = (0 until  size)
        .map {
            CurvePoint(
                identifier = fileName,
                pattern = patternData,
                probeNumber = it,
                position = labels[it],
                cpm = cpms[it],
                flagged = flags[it],
                meterRead = 0.0,
                ng = "")
        }
}