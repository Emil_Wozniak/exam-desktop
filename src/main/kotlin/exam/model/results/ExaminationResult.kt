package exam.model.results

import exam.model.enums.PointType

/**
 * Represents a type of result in all examined probes.
 *
 * @property identifier label, specific for examination
 * @property pattern used in examination type of pattern
 * @property probeNumber number id of examining probe (both Curve points and samples)
 * @property position name of examine probe (for Curve points [PointType]
 * and solution values, for samples current sample number.
 * @property cpm count per minute extracted from examine file
 * @property flagged if true instance is marked as unused during calculation
 * @property ng nanograms of examine type of the hormone
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
abstract class ExaminationResult(
    open var identifier: String,
    open val pattern: String,
    open val probeNumber: Int,
    open val position: String,
    open val cpm: Int,
    open var flagged: Boolean?,
    open var ng: String
)

