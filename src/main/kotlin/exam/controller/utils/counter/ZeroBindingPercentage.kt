package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.app.config.TO_PERCENT
import exam.controller.utils.math.Algorithms
import exam.model.curve.CalibrationCurveValues
import org.apache.commons.math3.util.Precision.round

object ZeroBindingPercentage : Algorithms {

    fun count(dimensions: CalibrationCurveValues): Double =
        runCatching { round(upTo100(dimensions) / byTotal(dimensions), 2) }
            .onSuccess {
                if (it > 20.0) println("Zero binding percentage finished with:${Ansi.COLOR.GREEN} $it ${Ansi.RESET}")
                else println("Zero binding percentage finished with:${Ansi.COLOR.RED} $it ${Ansi.RESET}")
            }
            .getOrElse { 0.0 }

    private fun upTo100(dimensions: CalibrationCurveValues): Double =
        (dimensions.binding.toDouble() * TO_PERCENT)

    private fun byTotal(dimensions: CalibrationCurveValues): Double = dimensions.total.toDouble()
}


