package exam.model.file

data class UploadFile(
    var title: String = "",
    var data: MutableCollection<String> = mutableListOf()
)