package exam.controller.utils.analizer.extractor

import exam.controller.utils.counter.Counter

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
fun nanograms(
    counter: Counter,
    cpms: List<Int>,
): List<Double> =
    counter.run {
        runCatching {
            countNGs(cpms.map { it.toDouble() }
                .toList())
        }
            .onSuccess { println("Result points NGs set successfully") }
            .onFailure { println("Couldn't set Result points NGs with cause: ${it.cause}") }
            .getOrElse { cpms.indices.map(Int::toDouble) }
    }
