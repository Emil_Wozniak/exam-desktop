package exam.controller.utils.analizer.extractor

import exam.model.curve.CurveDimensions
import exam.model.hormone.HormonePattern

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */


fun dimension(pattern: HormonePattern): CurveDimensions =
    CurveDimensions(pattern.totals, pattern.NSBs, pattern.Zeros)
