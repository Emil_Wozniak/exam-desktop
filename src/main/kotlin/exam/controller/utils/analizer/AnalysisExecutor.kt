package exam.controller.utils.analizer

import exam.model.results.ExaminationResults
import exam.model.setting.FileSettings
import java.io.File

interface AnalysisExecutor {
    fun analyze(uploadFile: File, settings: FileSettings): ExaminationResults
    fun recalculate(results: ExaminationResults): ExaminationResults
}