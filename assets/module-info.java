module exam {
    requires kotlin.stdlib;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires tornadofx;
    requires java.datatransfer;
    requires java.desktop;

    exports app;
    exports app.config;
    exports app.model;
    exports controller;
    exports controller.function;
    exports controller.utils.analizer;
    exports controller.utils.analizer.types;
    exports controller.utils.converter;
    exports controller.utils.counter;
    exports controller.utils.counter.types.calibrable;
    exports controller.utils.counter.types.countable;
    exports controller.utils.extractor;
    exports controller.utils.factory;
    exports controller.utils.math;
    exports controller.utils.pattern;
    exports controller.utils.reader;
    exports model.curve;
    exports model.enums;
    exports model.file;
    exports model.hormone;
    exports model.results;
    exports model.setting;
    exports model.table;
    exports view.buttons;
    exports view.components;
    exports view;
}