package pl.ejdev.analyze.service

trait CaseConversion {
    static def labeled(String text) {
        def normalized = text
                .trim()
                .replaceAll(/([^A-Z])([A-Z])/) { _, before, after -> "$before $after" }
        def parts = normalized.tokenize(/[-_\t\n ]+/) as String[]
        return parts.join(' ').toLowerCase()
    }
}