package exam.controller

import exam.app.config.*
import exam.model.file.UploadFile
import exam.controller.function.ResultPropertyController
import exam.controller.utils.analizer.FileAnalyzer
import exam.model.results.ExaminationResults
import exam.model.setting.FileSettings
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Alert.AlertType.ERROR
import javafx.scene.control.Alert.AlertType.WARNING
import javafx.scene.control.ButtonType.CLOSE
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File

class TableController : Controller() {

    private val analyzer: FileAnalyzer by inject()
    private val controller: ResultPropertyController by inject()

    private val settings = SimpleObjectProperty(FileSettings())
    val filename = SimpleStringProperty("")
    val savable = SimpleBooleanProperty(false)

    lateinit var exam: ExaminationResults
    private val file = UploadFile()
    private var fileChooser = FileChooser()

    fun uploadFile() {
        val uploadFile: File? = fileChooser.showOpenDialog(primaryStage)
        if (uploadFile == null) {
            alert(WARNING, NO_FILE, NO_FILE_INFO, CLOSE)
        }
        uploadFile?.run {
            filename.set(name)
            val content = readLines()
            val platesPosition = content.indexOf(ANALYSIS_VALUES_END_MARKER)
            when {
                platesPosition <= 36 -> alert(ERROR, FILE_ANALYSIS_FAILED, SHORT_FILE_INFO, CLOSE, title = SHORT_FILE)

                else -> run {
                    file.title = name
                    with(analyzer.analyze(this, settings.get())) {
                        this@TableController.exam = this
                        controller.setCurve(this.controlCurve)
                        controller.setPoints(this.resultPoints)
                        controller.setGraph(this.graph)
                        savable.set(true)
                    }
                }
            }
        }
    }

    fun setSettings(sets: FileSettings) = settings.set(sets)
}

