package exam.view.buttons

import exam.app.config.*
import exam.controller.TableController
import exam.controller.function.ResultPropertyController
import exam.model.table.Curve
import exam.model.table.Point
import exam.model.table.TableModel
import exam.view.components.material.materialDesignIcon
import io.github.palexdev.materialfx.controls.MFXButton
import javafx.geometry.Pos
import javafx.scene.control.Alert.AlertType.CONFIRMATION
import javafx.scene.control.Alert.AlertType.WARNING
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment.CENTER
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined.GREEN
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined.RED
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.kordamp.ikonli.materialdesign2.MaterialDesignD.DOWNLOAD
import tornadofx.*
import java.io.File
import java.io.FileOutputStream
import java.time.LocalDate.now

object XSLSWriter : View("") {
    private val controller: TableController by inject()
    private val results: ResultPropertyController by inject()

    private lateinit var workbook: XSSFWorkbook
    private lateinit var sheet: XSSFSheet

    override val root = VBox().apply {
        alignment = Pos.CENTER
        style { padding = box(4.px, 4.px) }
        this += MFXButton("XLSX").apply {
            textAlignment = CENTER
            graphic = materialDesignIcon(DOWNLOAD, 2.0, Color.BLACK)
            tooltip { text = SAVE }
            visibleWhen { controller.savable }
            setOnAction {
                if (results.curve.isNotEmpty()) {
                    chooseDirectory(SELECT_LOCATION)?.let {
                        val name = controller.filename.get().replace(FILE_FORMAT, REPLACEMENT) + now()
                        writeExcel(it, name)
                        alert(CONFIRMATION, "$SAVED $name.xlsx")
                    }
                } else alert(WARNING, NO_RESULTS, SELECT_FILE_ALERT)
            }
        }
    }

    private fun writeExcel(dir: File?, name: String) {
        workbook = XSSFWorkbook()
        sheet = workbook.createSheet(name)
        addToSheet(results.curve)
        addToSheet(results.points)
        dir?.let {
            FileOutputStream("${it.absolutePath}/${name}.xlsx").use { outputStream ->
                workbook.write(outputStream)
            }
        }
    }

    private fun addToSheet(points: List<TableModel>) {
        convert(points).withIndex().forEach { (index, line) ->
            val appendix = getRowIndexAppendix(points)
            with(sheet.createRow(index + appendix)) {
                line.withIndex().forEach { (count, field) ->
                    with(createCell(count + 1)) {
                        val style: CellStyle = workbook.createCellStyle()
                        val font: Font = workbook.createFont()
                        when (field) {
                            is String -> {
                                setCellValue(field)
                            }

                            is Int -> {
                                setCellValue(field.toDouble())
                            }

                            is Double -> {
                                setCellValue(field.toDouble())
                            }

                            is Boolean -> {
                                font.color = findColor(field)
                                style.setFont(font)
                                setCellStyle(style)
                                setCellValue(if (field == true) YES else NO)
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * When first element of points if of type [Curve] then 1 else [results] curve size + 2.
     *
     * curve size + 2 means that next row will be placed in [workbook], [sheet] will add 1
     * to not override curve last {@code index -1} and add 1 to override
     *
     * @param points current list of data to write in workbook
     * @return 1 when points contains [Curve] type or results.curve.size + 2
     */
    private fun getRowIndexAppendix(points: List<TableModel>) = when (points[0]) {
        is Curve -> 1
        else -> results.curve.size + 2
    }

    private fun findColor(field: Boolean): Short = if (!field) GREEN.index else RED.index

    private fun convert(points: List<TableModel>): MutableList<List<Any>> {
        val all = mutableListOf<List<Any>>()
        when (points[0]) {
            is Curve -> all.add(CURVE_HEADERS)
            else -> all.add(POINT_HEADERS)
        }
        all.addAll(createLines(points))
        return all
    }

    private fun createLines(points: List<TableModel>): List<List<Any>> = points.withIndex().mapNotNull {
        when (val point = it.value) {
            is Curve -> with(point) {
                listOf(probeNumber + 1, meterRead, position, cpm, flagged, it.index % 2)
            }

            is Point -> with(point) {
                listOf(probeNumber, average, ng, cpm, flagged, it.index % 2)
            }

            else -> null
        }
    }
}
