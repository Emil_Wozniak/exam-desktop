package exam.view

import exam.app.Styles.Companion.tableLabel
import exam.controller.function.ResultPropertyController
import javafx.geometry.Pos
import javafx.scene.layout.HBox
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

object ExaminationDetails : View("") {

    private val results: ResultPropertyController by inject()

    override val root = HBox().apply {
        val width = ScreenSize.width * 0.3 //600.0
        alignment = Pos.CENTER
        style { textAlignment = CENTER }
        prefWidth = width
        this += label(results.r) {
            padding = insets(2, 30, 2, 30)
            addClass(tableLabel)
        }
        this += label(results.zero) {
            padding = insets(2, 30, 2, 30)
            addClass(tableLabel)
        }
    }
}
