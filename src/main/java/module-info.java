module exam {
    requires tornadofx;
    requires java.desktop;
    requires kotlin.stdlib;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.base;
    requires java.base;
    requires org.apache.poi.poi;
    requires org.apache.poi.ooxml;
    requires commons.math3;
    requires cyclops;
    requires org.kordamp.ikonli.javafx;
    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.core;
    requires org.kordamp.ikonli.materialdesign2;
    requires MaterialFX;

    opens exam.view to javafx.fxml, javafx.graphics, javafx.base, javafx.controls, tornadofx;
    opens exam.app to
            javafx.fxml, javafx.graphics, javafx.base,
            javafx.controls, tornadofx, java.desktop,
            kotlin.reflect, kotlin.stdlib, java.base;

    exports exam.app;
    exports exam.app.config;
    exports exam.controller;
    exports exam.controller.function;
    exports exam.controller.utils.analizer;
    exports exam.controller.utils.analizer.extractor;
    exports exam.controller.utils.converter;
    exports exam.controller.utils.counter;
    exports exam.controller.utils.extractor;
    exports exam.controller.utils.math;
    exports exam.controller.utils.pattern;
    exports exam.controller.utils.reader;
    exports exam.model.curve;
    exports exam.model.enums;
    exports exam.model.file;
    exports exam.model.hormone;
    exports exam.model.results;
    exports exam.model.setting;
    exports exam.model.table;
    exports exam.view.buttons;
    exports exam.view.components.material;
    exports exam.view;
}