package exam.controller.utils.counter

class PointAverage(private val nanograms: List<Double>) {

    fun count(): List<Double> {
        val evens = eachSecond(nanograms, true)
        val odds = eachSecond(nanograms, false)
        val average = countAverage(evens, odds)

        return nanograms
            .mapIndexed { index, _ ->
                if (index % 2 == 0) average[index / 2]
                else 0.0
            }
            .toList()
    }

    private fun countAverage(evens: List<Int>, odds: List<Int>): List<Double> = evens
        .asSequence()
        .mapIndexed { index, even ->
            val odd = odds[index]
            if (even == 0 && odd == 0) 0
            else if (even == 0 && odd != 0) odd
            else if (even != 0 && odd == 0) even
            else (even + odd) / 2
        }
        .map { it.toDouble() }
        .map { it / 100 }
        .toList()

    private fun eachSecond(nanograms: List<Double>, even: Boolean): List<Int> = nanograms
        .asSequence()
        .map { it * 100 }
        .map { it.toInt() }
        .filterIndexed { index, _ ->
            if (even) index % 2 == 0
            else index % 2 == 1
        }
        .toList()
}
