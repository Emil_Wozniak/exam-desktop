package exam.view.components.material

import exam.model.table.TableModel
import javafx.beans.property.ReadOnlyListProperty
import javafx.event.EventTarget
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import org.kordamp.ikonli.Ikon
import org.kordamp.ikonli.javafx.StackedFontIcon
import tornadofx.*
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

@DslMarker
@Retention(AnnotationRetention.BINARY)
annotation class Components

@Components
inline fun <reified T> EventTarget.mdTable(
    items: ReadOnlyListProperty<T>,
    crossinline op: TableView<T>.() -> Unit = {}
) {
    tableview(items) {
        addClass("mfx-table-view")
        op(this)
    }
}

@Components
inline fun <reified S : TableModel, T> TableView<S>.mdColumn(
    name: String,
    prop: KMutableProperty1<S, T>,
    noinline dsl: TableColumn<S, T>.() -> Unit = {}
): TableColumn<S, T> =
    this.column(name, prop) {
        addClass("mfx-table-column")
        dsl(this)
    }

@Components
inline fun <reified S, T> TableView<S>.mdReadonlyColumn(
    title: String,
    prop: KProperty1<S, T>,
    noinline op: TableColumn<S, T>.() -> Unit = {}
): TableColumn<S, T> =
    this.readonlyColumn(title, prop) {
        addClass("mfx-table-column")
        op(this)
    }

@Components
fun materialDesignIcon(icon: Ikon, size: Double = 1.0, color: Color? = Color.BLACK): StackPane =
    StackedFontIcon().apply {
        setIconCodes(icon)
        setIconSizes(size)
        iconColor = color
    }