package exam.controller.utils.extractor

import exam.app.config.Ansi
import exam.app.config.COLUMN_SPLICER
import exam.app.config.NEGATIVE_ONE
import exam.model.results.ExaminationResult
import tornadofx.*

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class TxtDataExtractor : Controller(), DataExtractor {

    /**
     * Extracts [ExaminationResult.cpm] from txt file.
     * It will split row to the list of string and try find
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     * @return list of string, where each
     */
    override fun getColumn(line: String, column: Int): String =
        line.split(COLUMN_SPLICER)
            .toList()
            .runCatching { get(column) }
            .onFailure { println("${Ansi.COLOR.RED}Row does not contain enough amount of column:${Ansi.RESET} $it") }
            .getOrElse { NEGATIVE_ONE }

}