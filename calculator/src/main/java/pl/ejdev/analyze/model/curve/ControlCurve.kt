package pl.ejdev.analyze.model.curve

data class CurveReadValues(
    val totals: List<Int>,
    /**
     * Zeros
     */
    val bg: List<Int>,
    /**
     * NSBS
     */
    val bo: List<Int>
)

data class ControlCurve(
    val totals: Double,
    val zeros: Double,
    val nsbs: Double,
    val binding: Double
)