package exam.view

import exam.view.buttons.AddFile
import exam.view.buttons.XSLSWriter
import javafx.geometry.Pos.CENTER
import javafx.scene.Parent
import javafx.scene.layout.FlowPane
import javafx.scene.layout.VBox
import tornadofx.*

object Sidebar : View() {
    override val root: Parent = FlowPane().apply {
        with(ScreenSize) {
            prefWidth = width * 0.3 //600.0
            prefHeight = height * 0.8 //1800.0
        }
        padding = insets(2, 0, 2, 2)
        this.add(VBox().apply {
            this += hbox {
                this += borderpane {
                    center = ExaminationDetails.root
                    bottom = ResultChart.root
                }
            }
            this += hbox {
                this.alignment = CENTER
                this.borderpane {
                    this.left = AddFile.root
                    this.center = XSLSWriter.root
                    this.bottom = FormView.root
                }
            }
        })
    }
}