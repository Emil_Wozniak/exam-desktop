package exam.controller.utils.converter

import exam.model.file.ExaminationFile
import exam.model.setting.FileSettings
import java.io.File

interface SimpleConverter {
    /**
     * @param file     uploaded file wrapped on ExaminationFile
     * @param settings values to determine HormonePattern
     * @return instance of [ExaminationFile]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun convert(file: File, settings: FileSettings): ExaminationFile
}