package exam.model.hormone


/**
 *  pattern "2;3;3;7;2;0"
 */
data class CortisolPattern(
    override var totals: Int = 2,
    override var NSBs: Int = 3,
    override var Zeros: Int = 3,
    override var length: Int = 7,
    override var repeats: Int = 2,
    override var startValue: Double = 1.25,
    override var points: List<Point> = mutableListOf(),
    override var controlPoints: Int = 0
) : HormonePattern(
    totals,
    NSBs,
    Zeros,
    length,
    repeats,
    startValue,
    points,
    controlPoints
) {
    constructor() : this(points = mutableListOf()) {
        this.points = points(
            patternValues = mutableListOf(totals, NSBs, Zeros, length, repeats, controlPoints),
            startValue
        )
    }
}
