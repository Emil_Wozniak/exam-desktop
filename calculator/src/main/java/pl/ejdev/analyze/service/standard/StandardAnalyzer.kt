package pl.ejdev.analyze.service.standard

import org.apache.commons.math3.util.Precision.round
import pl.ejdev.analyze.model.standard.StandardPoint
import java.math.BigDecimal
import kotlin.math.log10

internal object StandardAnalyzer {
    private const val ROUND_HALF_EVEN = 6

    fun analyze(points: List<Double>, cpms: List<Int>, zero: Double, binding: Double): List<StandardPoint> {
        require(cpms.size == points.size) { "Incorrect size of cpms: ${cpms.size} expected ${points.size}" }

        return points.mapIndexed { index, std ->
            val cpm = cpms[index]
            val bindFirstElement: Double = (cpm - zero) * 100
            val bindingPercent: Double = bindFirstElement / binding
            val logDose: Double = log10(std)
            val logarithmRealZero: Double = log10(bindingPercent / (100 - bindingPercent))
            StandardPoint(
                std = std,
                cpm = cpm,
                bindingPercent = round(bindingPercent, 0),
                logDose = round(logDose, 1),
                logarithmRealZero = round(logarithmRealZero, 2, ROUND_HALF_EVEN)
            )
        }
    }
}