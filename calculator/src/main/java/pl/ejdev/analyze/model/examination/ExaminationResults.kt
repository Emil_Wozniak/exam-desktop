package pl.ejdev.analyze.model.examination

import pl.ejdev.analyze.model.regression.RegressionParameters
import pl.ejdev.analyze.model.standard.StandardPoint

data class ExaminationResults(
    val samples: List<SamplePoint>,
    val standard: List<StandardPoint>,
    val regressionParameters: RegressionParameters
)

data class SamplePoint(
    val nr: Int,
    val cpm: Int,
    val ngPerMl: Double,
    val avg: Double,
    val cv: Double,
    val sample: Int
)