@file:Suppress("LocalVariableName")

package exam.model.hormone

import exam.model.enums.PointType
import exam.model.setting.FileSettings

/**
 * @author emil.wozniak.591986@gmail.com
 * @property totals
 * @property NSBs non specific binding values
 * @property Zeros amount of blind points on standard
 * @property length amount of expected standard points
 * @property repeats amount of repeats of standard point
 * @property startValue value of first point of the standard pattern
 * @property points list of generated points
 * @since 15.06.2020
 * example standard pattern:
 * [1.25, 1.25, 2.5, 2.5, 5.0, 5.0, 10.0, 10.0, 20.0, 20.0, 40.0, 40.0, 80.0, 80.0]
 * length = 7
 * repeats = 2
 * startValue = 1.25
 */
abstract class HormonePattern(
    open var totals: Int,
    open var NSBs: Int,
    open var Zeros: Int,
    open var length: Int,
    open var repeats: Int,
    open var startValue: Double,
    open var points: List<Point>,
    open var controlPoints: Int
) : PointBuilder {
    companion object {
        fun resolve(settings: FileSettings): HormonePattern = analysisFactory(settings)
        /**
         * Takes [FileSettings.defaults] value, if it's true then returns value.
         *
         * @since 15.06.2020
         * @author emil.wozniak.591986@gmail.com
         */
        private val analysisFactory: (FileSettings) -> HormonePattern = {
            if (it.defaults) CortisolPattern()
            else isCustomPattern(it)
        }

        private fun isCustomPattern(settings: FileSettings): HormonePattern {
            val (_, pattern, startValue) = settings
            return CustomPattern(pattern, startValue.toDouble())
        }
    }
}

/**
 * @property name PointType name
 * @property value
 * @see PointType
 */
class Point(
    val name: String,
    val value: Double
)


