package exam.controller.utils.analizer

import exam.app.config.DEFAULT_COLUMN
import exam.controller.utils.analizer.extractor.*
import exam.controller.utils.counter.BaseCounter
import exam.controller.utils.counter.Counter
import exam.controller.utils.extractor.DataExtractor
import exam.controller.utils.extractor.TxtDataExtractor
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint
import tornadofx.*

/**
 * @see ResultsAnalyzer
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
object CurveAnalyzer {

    /**
     * Setup params required to instantiate [CurvePoint].
     *
     * @param pattern instance of [HormonePattern], which is provided from [ResultsAnalyzer].
     * @param data extracted strings from uploaded file
     * @param columnNumber when null [TxtDataExtractor] will point column 3 of data results
     * as default in another way it will override default.
     *
     * @return list of CurvePoint instances.
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun analyze(
        data: List<String>,
        pattern: HormonePattern,
        counter: Counter,
        extractor: DataExtractor,
        columnNumber: Int? = null
    ): List<CurvePoint> {
        val dimensions = dimension(pattern)
        val metadata = metadata(data)
        val cpms = cpms(extractor, metadata, columnNumber ?: DEFAULT_COLUMN)
        val fileName = filename(data)
        val patternData = patternData(data)
        val labels = labels(pattern)
        val flags = flags(dimensions, pattern, cpms)
        val controlCurve: List<CurvePoint> = ControlCurveCreator
            .createPoints(
                pattern.points.size,
                fileName,
                patternData,
                labels,
                cpms,
                flags
            )

        return assignMeterToCurve(
            controlCurve,
            counter,
            dimensions,
            pattern
        )
    }
}
