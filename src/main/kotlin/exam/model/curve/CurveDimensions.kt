package exam.model.curve

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
@Suppress("PropertyName")
data class CurveDimensions(
    val T: Int,
    val N: Int,
    val O: Int
) {

    val all: Int = T + N + O

    fun zeroStart(): Int = T

    fun zeroEnd(): Int = N + T

    fun nsbsEnd(): Int = O + N + T

}