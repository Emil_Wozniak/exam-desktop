package pl.ejdev.analyze.service.regression

import mu.KotlinLogging
import org.apache.commons.math3.util.Precision.round
import pl.ejdev.analyze.model.regression.RegressionParameters
import pl.ejdev.analyze.model.standard.StandardPoint
import pl.ejdev.analyze.utils.`correlation coefficient`
import pl.ejdev.analyze.utils.`sum product`
import pl.ejdev.analyze.utils.sumsq
import kotlin.math.log10
import kotlin.math.pow

internal object RegressionAnalyzer {
    private val logger = KotlinLogging.logger { }

    fun analyze(standardPoints: List<StandardPoint>, zero: Double, nsbs: Double): RegressionParameters {
        val logDoses = standardPoints.map(StandardPoint::logDose).map { round(it, 1) }
        val logarithmRealZeros = standardPoints.map(StandardPoint::logarithmRealZero)
        val cpms = standardPoints.map(StandardPoint::cpm)
        val b = calculateParameterB(logDoses, logarithmRealZeros)
        val a = calculateParameterA(logarithmRealZeros, logDoses, b)
        val r = `correlation coefficient`(logDoses, logarithmRealZeros)
        val n = logDoses.count()
        val pgs = cpms.map { calculatePg(it, zero, nsbs, a, b) }
        val graph = GraphService.createGraph(logDoses, logarithmRealZeros, pgs, cpms)
        return RegressionParameters(n, b, a, r, graph)
    }

    /**
     * ```
     * = (
     *  COUNT(logDose) * SUMPRODUCT(logDose,logarithmRealZero) - SUM(logDose) * SUM(logarithmRealZero)
     *  ) / (
     *  COUNT(logDose) * SUMSQ(logDose) - ( SUM(logDose) )^2
     *  )
     * ```
     */
    private fun calculateParameterB(logDoses: List<Double>, logarithmRealZeros: List<Double>): Double {
        val logDoseSum = logDoses.sum()
        val paramB = (logDoses.size * `sum product`(logDoses, logarithmRealZeros) -
                logDoseSum * logarithmRealZeros.sum()) /
                (logDoses.size * logDoses.sumsq() - logDoseSum.pow(2.0))
        logger.warn { "Regression Parameter B = $paramB" }
        return paramB
    }

    /**
     * ```
     * = SUM(logarithmRealZero) / COUNT(logDose) - Regression_param_b * SUM(logDose) / COUNT(logDose)
     * ```
     */
    private fun calculateParameterA(logarithmRealZeros: List<Double>, logDoses: List<Double>, b: Double): Double {
        val paramA = logarithmRealZeros.sum() / logDoses.size - b * logDoses.sum() / logDoses.size
        logger.warn { "Regression Parameter A = $paramA" }
        return paramA
    }


    private fun calculatePg(cpm: Int, zero: Double, nsbs: Double, a: Double, b: Double): Double {
        val cpmMinusZero = round(cpm - zero, 2)
        val times10 = round(cpmMinusZero * 100, 2)
        val divNsbs = round(times10 / nsbs, 2)
        val minus100 = 100 - divNsbs
        val log10 = round(log10(divNsbs / minus100), 2)
        val ending = round(log10 - a, 2) / b
        return 10.times(ending)
    }
}