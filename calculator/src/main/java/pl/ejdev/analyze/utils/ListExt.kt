package pl.ejdev.analyze.utils

fun <T> List<T>.copyEach(repeats: Int, action: (T) -> T = { it }): List<T> =
    this.flatMap { (0 until repeats).map { _ -> action(it) } }