package pl.ejdev.analyze.utils

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation
import org.apache.commons.math3.util.Precision
import kotlin.math.pow
import kotlin.math.sqrt

@Suppress("SpellCheckingInspection")
private val pearsonsCorrelation = PearsonsCorrelation()


/**
 *  the sum of squares
 */
@Suppress("SpellCheckingInspection")
internal fun List<Double>.sumsq(): Double = this.sumOf { i -> i.pow(2) }


@Suppress("FunctionName")
internal fun `correlation coefficient`(x: List<Double>, y: List<Double>, scale: Int = 4): Double =
    pearsonsCorrelation
        .correlation(
            /* xArray = */ x.toDoubleArray(),
            /* yArray = */ y.toDoubleArray()
        )
        .let { Precision.round(it, scale) }

@Suppress("FunctionName")
internal fun `sum product`(x: List<Double>, y: List<Double>): Double {
    require(x.size == y.size) { "lists size are not equals ${x.size} != ${y.size}" }
    return x.indices.sumOf { x[it] * y[it] }
}

internal fun Collection<Double>.stdDevDouble(): Double {
    val mean = this.sum() / this.size

    var standardDeviation = 0.0
    this.forEach { standardDeviation += (it - mean).pow(2.0) }

    return sqrt(standardDeviation / this.size)
}

internal fun Collection<Number>.stdDev(): Double {
    return this.map { it.toDouble() }.stdDevDouble()
}
