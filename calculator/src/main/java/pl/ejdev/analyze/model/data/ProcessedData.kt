package pl.ejdev.analyze.model.data

import pl.ejdev.analyze.model.curve.CurveReadValues

data class ProcessedData(
    val curveLines: List<Int>,
    val standardLines: List<Int>,
    val samples: List<Int>,
    val settings: DataSettings = DataSettings.default
) {
    constructor(
        lines: List<Int>,
        settings: DataSettings = DataSettings.default
    ) : this(
        settings = settings,
        curveLines = lines
            .take(settings.curve.totals + settings.curve.bg + settings.curve.bo),
        standardLines = lines
            .drop(settings.curve.totals)
            .drop(settings.curve.bg)
            .drop(settings.curve.bo)
            .take(settings.curve.standardSize),
        samples = lines
            .drop(settings.curve.totals)
            .drop(settings.curve.bg)
            .drop(settings.curve.bo)
            .drop(settings.curve.standardSize),
    )

    fun createCurveReadValues(): CurveReadValues = CurveReadValues(
        totals = curveLines
            .take(settings.curve.totals),
        bg = curveLines
            .drop(settings.curve.totals)
            .take(settings.curve.bg),
        bo = curveLines
            .drop(settings.curve.totals + settings.curve.bg)
            .take(settings.curve.bo),
    )
}

data class DataSettings(
    val repeats: Int = 2,
    val curve: CurveSettings = CurveSettings()
) {
    companion object {
        val default = DataSettings()
    }
}

data class CurveSettings(
    val totals: Int = 2,
    val bg: Int = 2,
    val bo: Int = 3,
    val standardSize: Int = 14
) {
    val size: Int = totals + bg + bo + standardSize
}
