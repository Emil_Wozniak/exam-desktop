package exam.controller.utils.analizer.extractor

import exam.controller.utils.extractor.DataExtractor

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 * @return list of count per minute values extracted from file
 */
fun cpms(
    extractor: DataExtractor,
    metadata: List<String>,
    columnNr: Int
): List<Int> = runCatching {
    metadata
        .asSequence()
        .filter { it.startsWith(" \tUnk") }
        .map { extractor.getColumn(it, columnNr) }
        .map(String::toInt)
        .toList()
}
    .getOrElse { emptyList() }

