package exam.controller.utils.analizer.extractor

    fun filename(data: List<String>): String = data[0].trim { it <= ' ' }
