package exam.controller.utils.counter

import exam.model.curve.CalibrationCurveValues
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint

/**
 * Provides methods to perform calculations and get return values.
 * @since 13.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface Counter {

    var dose: List<Double>
    var realZero: List<Double>
    var zeroBinding: Double
    var correlation: Double
    var calibrationCurveValues: CalibrationCurveValues
    var standards: MutableList<Int>
    var paramB: Double
    var paramA: Double
    var bindingPercentage: List<Double>

    fun countMeters(pattern: HormonePattern, curve: List<CurvePoint>): List<Double>

    fun calculateDoseLogarithm(pattern: HormonePattern): CalibrationCurveValues

    fun countNGs(cpms: List<Double>): List<Double>

}