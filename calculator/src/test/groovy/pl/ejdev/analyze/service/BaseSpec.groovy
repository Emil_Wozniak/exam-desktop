package pl.ejdev.analyze.service

import groovy.util.logging.Slf4j
import pl.ejdev.analyze.model.data.DataSettings
import spock.lang.Shared
import spock.lang.Specification

@Slf4j
abstract class BaseSpec extends Specification implements CaseConversion {

    @Shared
    protected List<Double> STANDARD_POINTS = [1.25, 1.25, 2.5, 2.5, 5.0, 5.0, 10.0, 10.0, 20.0, 20.0, 40.0, 40.0, 80.0, 80.0]

    @Shared
    protected List<Integer> cpms = [412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74, 65]

    @Shared
    protected List<Double> bindingPercentage = [90, 85, 82, 76, 68, 67, 45, 51, 31, 33, 16, 19, 10, 7].collect { it.toDouble() }

    @Shared
    protected List<Double> logDose = [0.10, 0.10, 0.40, 0.40, 0.70, 0.70, 1.00, 1.00, 1.30, 1.30, 1.60, 1.60, 1.90, 1.90]

    // TODO in excel:
    //        [0.95, 0.74, 0.65, 0.49, 0.34, 0.31, -0.08, 0.02, -0.34, -0.30, -0.71, -0.62, -0.98, -1.10]
    //           X     V     V     V    V      V     V      V     X      V      X       V     X     V
    @Shared
    protected List<Double> logarithmRealZero = [0.94, 0.74, 0.65, 0.49, 0.34, 0.31, -0.08, 0.02, -0.35, -0.3, -0.72, -0.62, -0.98, -1.1]

    protected final static BINDING = 421.0
    protected final static CURVE_ZERO_VALUE = 34.0

    protected def excelMismatch(Closure<Map<Object, Object>> body) {
        def entry = body().entrySet().first()
        log.error("in excel $entry.key but it is $entry.value")
    }

    @Shared
    protected def settings = DataSettings.default

    @Shared
    protected List<Integer> lines = [
            1976, 1982,
            32, 36,
            458, 459, 447,
            412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74, 65,
            219, 224,
            164, 191,
            226, 230,
            293, 279,
            325, 347,
            386, 388,
            372, 379,
            293, 330,
            252, 237,
            225, 247,
            211, 220,
            313, 295,
            338, 341,
    ]
}