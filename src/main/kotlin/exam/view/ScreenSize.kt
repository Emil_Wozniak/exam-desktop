package exam.view

import java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment

object ScreenSize {
    var width: Double = 800.0
    var height: Double = 640.0

    init {
       getLocalGraphicsEnvironment().defaultScreenDevice.displayMode.let {
            width = it.width.toDouble()
            height = it.height.toDouble()
        }
    }
}
