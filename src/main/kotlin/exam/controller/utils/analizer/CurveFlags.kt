package exam.controller.utils.analizer

import exam.app.config.Ansi.AnsiCode.RESET
import exam.app.config.Ansi.COLOR
import exam.controller.utils.counter.SpreadCounter
import exam.model.curve.CurveDimensions
import exam.model.hormone.HormonePattern

fun flags(
    dimensions: CurveDimensions,
    pattern: HormonePattern,
    cpms: List<Int>,
): List<Boolean> =
    runCatching { (0 until pattern.points.size).map { getFlag(it, dimensions, cpms) } }
        .onSuccess { println("Curve Flagged: ${it.map(::collectResults)}") }
        .onFailure { println("Curve flagging failed with: ${it.message}") }
        .getOrElse { ArrayList() }

fun collectResults(flagged: Boolean) =
    if (flagged) "${COLOR.RED}$flagged$RESET"
    else "${COLOR.GREEN}$flagged$RESET"

fun getFlag(index: Int, dimensions: CurveDimensions, cpms: List<Int>): Boolean =
    dimensions.run {
        val zerosFlags = countFlags(cpms, zeroStart(), O)
        val nsbsFlags = countFlags(cpms, zeroEnd(), N)
        return if (index < zeroStart()) false
        else if (index == zeroStart() || index < zeroEnd()) zerosFlags[index - zeroStart()]
        else if (index < nsbsEnd()) nsbsFlags[index - zeroEnd()]
        else false
    }

fun countFlags(cpms: List<Int>, from: Int, to: Int): List<Boolean> =
    SpreadCounter.isSpread(cpms.drop(from).take(to))

