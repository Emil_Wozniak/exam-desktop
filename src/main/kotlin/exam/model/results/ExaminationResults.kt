package exam.model.results

import exam.model.hormone.HormonePattern
import exam.model.setting.FileSettings

/**
 * All types of results container.
 *
 * @property title filename
 * @property patternTitle default or provided by [FileSettings] pattern name
 * @property controlCurve Calibration Curve points
 * @property resultPoints results of calculation of the samples
 * @property graph properties required to draw graph
 *
 * @since 16.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class ExaminationResults(
    var title: String,
    var patternTitle: String,
    val controlCurve: List<CurvePoint?>,
    val resultPoints: List<ExaminationPoint?>,
    val graph: BaseGraph? = null,
    val pattern: HormonePattern
)