package pl.ejdev.analyze.service.regression

import org.apache.commons.math3.util.Precision
import pl.ejdev.analyze.model.regression.RegressionParameters
import pl.ejdev.analyze.utils.copyEach

internal object GraphService {
    fun createGraph(
        logDoses: List<Double>,
        logarithmRealZeros: List<Double>,
        pgs: List<Double>,
        cpms: List<Int>
    ): RegressionParameters.Graph {
        logDoses.size
        val nrs = (0 until logDoses.size / 2).toList().copyEach(2)
        val points = logDoses.indices.map { index ->
            val pg: Double = pgs[index]
            val std: Int = cpms[index]
            RegressionParameters.Point(
                x = logDoses[index],
                y = logarithmRealZeros[index],
                pg = pg,
                nr = nrs[index],
                deltaPercentage = deltaPercentage(pg = pg, std = std),
                ngTimesN = ngTimesN(pg = pg, std = std)
            )
        }
        return RegressionParameters.Graph(points)
    }

    private fun ngTimesN(pg: Double, std: Int): Double = pg / std


    private fun deltaPercentage(pg: Double, std: Int): Int = Precision.round((pg - std) * 100 / std, 0).toInt()
}