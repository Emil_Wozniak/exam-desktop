module exam {
    requires transitive tornadofx;
    requires transitive kotlin.reflect;
    requires transitive kotlin.stdlib;
    requires transitive javafx.base;
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    requires transitive javafx.fxml;
    requires transitive java.logging;
    requires transitive org.apache.poi.ooxml;
    requires transitive batik.all;
    opens app to javafx.fxml,
            tornadofx,
            kotlin.reflect,
            kotlin.stdlib,
            javafx.base,
            javafx.controls,
            javafx.graphics,
            java.logging,
            org.apache.poi.ooxml,
            org.apache.commons.logging,
            commons.io;
    exports app;

}