//noinspection GrUnresolvedAccess
runner {
    println '=> Init Spock configs'
    filterStackTrace false
    setReport()
}

@SuppressWarnings(['GrUnresolvedAccess', 'GroovyGStringKey'])
private void setReport() {
    final root = 'com.athaydes.spockframework.report'
    println '=> Set report'
    final configs = [
            "${root}.showCodeBlocks"                             : true,
            "${root}.outputDir"                                  : 'build/spock/reports',
            "${root}.internal.HtmlReportCreator.featureReportCss": 'templates/spock/feature-report.css',
            "${root}.internal.HtmlReportCreator.summaryReportCss": 'templates/spock/summary-report.css'
    ].tap { Map config -> config.each { key, value -> println "==> $key: $value" } }
    spockReports { set(configs) }
}
