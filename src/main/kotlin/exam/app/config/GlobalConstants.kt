package exam.app.config

const val SHORT_FILE_INFO = """
Upewnij się że wybrany plik zawiera przynajmniej 36 linii tekstu.

Tekst powinien być zgodny z poniższym wzorem:
1. Położenie pliku, 1 linia: 'C:\mbw\results\A16_501.txt' 
2. Informacje o analizie: 5 linii tekstu zawierające
- RUN INFORMATION:
- ================
- Counting protocol no:
- Name:
- CPM normalization protocol no:
3. 2 puste linie
4. Kolumny:
- 1 linia z: COLUMNS:
- 1 linia: ========
- 1 linia: nazwy kolumn: ' 	SAMPLE	POS	CCPM1	CCPM1%'
- przynajmniej 26 linii zaczynających się od ' 	Unk'
"""

const val WRONG_FORMAT = "Zły format pliku"
const val CORRECT_FORMAT = "txt"
const val CORRECT_EXT = "Proszę wybrać plik z rozszerzeniem .txt"
const val FILE_ANALYSIS_FAILED = "Analiza pliku się nie powiodła"
const val SHORT_FILE = "Plik jest za krótki"
const val NO_FILE = "Plik z wynikami nie został wybrany"
const val NO_FILE_INFO = "Po wybraniu pliku nastąpi analiza wyników"

const val ANALYSIS_VALUES_END_MARKER = "PLATES:"

const val ADD_FILE = "Dodaj plik z wynikami"
const val CHART = "Wykres"
const val POINT = "Punkt"
const val RESULT = "Wynik"
const val SAVE = "Zapisz"
const val SELECT_LOCATION = "Wybierz miejsce zapisu"
const val NO_RESULTS = "Brak wyników do zapisania"
const val SELECT_FILE_ALERT = "Proszę dodać najpierw plik z wynikami"

const val SAVED = "Zapisano "
const val YES = "Tak"
const val NO = "Nie"

val CURVE_HEADERS = listOf<Any>("nr", "meter read", "position", "cpm", "flagged", "filter")
val POINT_HEADERS = listOf<Any>("nr", "average", "ng", "cpm", "flagged", "filter")

const val CUSTOM_SETTINGS = "Niestandardowe ustawienia "
const val ANALYSIS_SETTINGS = "Ustawienia analizy "
const val CUSTOM_CURVE = "Punkty krzywej "
const val CUSTOM_CURVE_TOOLTIP =
    "W kolejności: ilość Totali, NSB, Zero, długość standardów, liczba powtórzeń, ilość kontroli"
const val CUSTOM_CURVE_LOWEST = "Najniższa wartość krzywej "
const val SUBMIT = "Zatwierdź"

const val CALIBRATION_CURVE = "Krzywa kalibracyjna"
const val POINT_RESULTS = "Wyniki próbek"

const val FILE_FORMAT = ".txt"
const val REPLACEMENT = "_"

val BTN_INFO = arrayOf("btn", "btn-info")
const val FORM_CONTROL = "form-control"

const val SERIES_1 = "linia 1"
const val SERIES_2 = "linia 2"

const val NR = "NR"
const val POSITION= "Pozycja"
const val MEASUREMENT = "Pomiar"
const val FLAG = "Flaga"
const val CPM = "CPM"
const val NGS = "NG"
const val AVG = "AVG"