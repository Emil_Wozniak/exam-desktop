package exam.controller.utils.converter

import exam.app.config.CORTISOL_STANDARD
import exam.app.config.DEFAULT_TARGET_POINT
import exam.model.file.ExaminationFile
import exam.model.setting.FileSettings
import tornadofx.*
import java.io.File

class FileConverter : Controller(), SimpleConverter {
    /**
     * @param file     uploaded file wrapped on ExaminationFile
     * @param settings values to determine HormonePattern
     * @return instance of [ExaminationFile]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun convert(file: File, settings: FileSettings): ExaminationFile =
        with(settings) {
            val target = extractValue(target, DEFAULT_TARGET_POINT)
            val pattern = extractValue(pattern, CORTISOL_STANDARD)
            ExaminationFile(file, target, pattern, defaults)
        }

    /**
     * in another way a target value.
     */
    private fun extractValue(target: String?, defaultValue: String): String {
        return if (target == null || target == "") defaultValue
        else target
    }
}