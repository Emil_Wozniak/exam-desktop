package exam.controller.utils.reader

import exam.model.file.ExaminationFile

/**
 * Facade
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface FileReader {
    /**
     * Analyze content of [data].
     *
     * @param data uploaded file
     * @return Strings containing [data] for further analysis
     */
    fun read(data: ExaminationFile): List<String>
}
