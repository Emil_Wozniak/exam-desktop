package exam.model.hormone

import exam.model.enums.PointType
import exam.model.enums.PointType.*

interface PointBuilder {
    /**
     * @param patternValues each return entity values
     * @return list of [Point] in range of
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    fun points(patternValues: List<Int>, startValue: Double): List<Point> {
        val points: MutableList<Point> = mutableListOf()
        points.addAll(createPoint(patternValues[0], T))
        points.addAll(createPoint(patternValues[1], NSB))
        points.addAll(createPoint(patternValues[2], ZERO))
        points.addAll(createStandardPattern(patternValues, startValue))
        points.addAll(createPoint(patternValues[5], K))
        return points
    }

    private fun createPoint(amount: Int, type: PointType): List<Point> = (0 until amount).map { Point(type.name, 0.0) }

    private fun createStandardPattern(patternValues: List<Int>, startValue: Double): List<Point> {
        val points = populatePoints(patternValues[3], startValue)
        return handleDuplicates(patternValues[4], points).map { Point(it.toString(), it) }
    }

    /**
     * @param length     number of standard points
     * @param startValue the lowest value of a standard pattern
     * @return list of standard hormone values
     *
     *
     * takes defined [HormonePattern.length] and
     * [HormonePattern.startValue],
     * then generates each next value for a standard in order that each next should be `2x startValue`
     * @author emil.wozniak.591986@gmail.com
     * @see HormonePattern.length
     * @see HormonePattern.startValue
     * @receiver HormonePattern
     *
     * @since 14.06.2020
     * @return Hormone pattern points
     */
    private fun populatePoints(length: Int, startValue: Double): List<Double> {
        var current = 1
        return (0 until length)
            .map {
                if (it == 0) 1 else {
                    current += current
                    current
                }
            }
            .map { it * startValue }
    }

    /**
     * @param repeats number of repeats of the standard point
     * @param points  list of points
     * @return the list of points which each Point will have requested amount of duplicates
     * Process will be skipped if repeats equals 1 or less
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    private fun handleDuplicates(repeats: Int, points: List<Double>): List<Double> =
        if (repeats > 1) points.map { duplicate(repeats, it) }.flatten()
        else points

    /**
     * @param repeats number of repeats of the standard point
     * @param value   current duplicated value
     * @return list of same values
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    private fun duplicate(repeats: Int, value: Double): List<Double> = (0 until repeats).map { value }
}
