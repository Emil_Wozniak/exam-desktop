package exam.model.table

import exam.model.results.ExaminationPoint
import tornadofx.property

data class Point(val point: ExaminationPoint?): TableModel {
    private val number = point?.probeNumber ?: 0
    val average: Double by property(point?.average ?: 0.0)
    val pattern: String by property(point?.pattern ?: "")
    val probeNumber: Int by property(number + 1)
    val cpm: Int by property(point?.cpm ?: 0)
    val flagged: Boolean by property(point?.flagged ?: false)
    val ng: String by property(point?.ng ?: "")

    fun toPoint() = point
}