package exam.model.table

import exam.model.results.CurvePoint
import tornadofx.getProperty
import tornadofx.property

data class Curve(
  val point: CurvePoint
) : TableModel {
    var meterRead: Double by property(point.meterRead)
    fun meterReadProp() = getProperty(Curve::meterRead)

    var pattern: String by property(point.pattern)
    fun patternProp() = getProperty(Curve::meterRead)

    var probeNumber: Int by property(point.probeNumber)
    fun probeNumberProp() = getProperty(Curve::probeNumber)

    var position: String by property(point.position)
    fun positionProp() = getProperty(Curve::position)

    var cpm: Int by property(point.cpm)
    fun cpmProp() = getProperty(Curve::cpm)

    var flagged: Boolean by property(point.flagged)
    fun flaggedProp() = getProperty(Curve::flagged)

    override fun toString(): String {
        return "Curve(meterRead=$meterRead, pattern='$pattern', probeNumber=$probeNumber, position='$position', cpm=$cpm, flagged=$flagged)"
    }

    fun toCurvePoint() = CurvePoint(meterRead, "", pattern, probeNumber, position, cpm, flagged, "")
}