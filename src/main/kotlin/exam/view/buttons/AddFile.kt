package exam.view.buttons

import exam.app.config.ADD_FILE
import exam.controller.TableController
import exam.view.components.material.materialDesignIcon
import io.github.palexdev.materialfx.controls.MFXButton
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment.CENTER
import org.kordamp.ikonli.materialdesign2.MaterialDesignU
import tornadofx.*

object AddFile : View("") {
    private val controller: TableController by inject()
    override val root = VBox().apply {
        style {
            padding = box(4.px, 4.px)
        }
        this += MFXButton("Dodaj plik").apply {
            id = "add-file-button"
            graphic = materialDesignIcon(MaterialDesignU.UPLOAD, 2.0, Color.BLACK)
            textAlignment = CENTER
            tooltip { text = ADD_FILE }
            action { controller.uploadFile() }
        }
    }
}
