package exam.controller.utils.analizer

import exam.controller.utils.counter.BaseCounter
import exam.controller.utils.counter.Counter
import exam.controller.utils.extractor.DataExtractor
import exam.controller.utils.extractor.TxtDataExtractor
import exam.controller.utils.pattern.HormoneStrategy
import exam.controller.utils.reader.FileReader
import exam.model.hormone.HormonePattern
import exam.model.results.*
import tornadofx.*

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class ResultCalculationAnalyzer : ResultsAnalyzer, Controller() {

    private val counter: Counter by inject<BaseCounter>()
    private val extractor: DataExtractor by inject<TxtDataExtractor>()
    private val pointsAnalyzer by inject<PointsAnalyzer>()

    override fun analyze(
        data: List<String>,
        pattern: HormonePattern,
        name: String,
        patternTitle: String
    ): ExaminationResults {
        val curve: List<CurvePoint> = createCurve(data, pattern)
        val points: List<ExaminationPoint> = createPoints(data, pattern, curve)
        val graph: BaseGraph = createGraph(pattern)
        return ExaminationResults(name, patternTitle, curve, points, graph, pattern)
    }

    /**
     * @param data separated from uploaded file by [FileReader.read]
     * @param pattern instance of [HormonePattern]
     */
    private fun createCurve(data: List<String>, pattern: HormonePattern): List<CurvePoint> =
        CurveAnalyzer.analyze(data, pattern, counter, extractor)

    /**
     * @param data separated from uploaded file by [FileReader.read]
     * @param pattern instance of [HormonePattern]
     * @return instance of {@link PointAnalyzer}
     * @see FileReader
     * @see HormoneStrategy
     */
    private fun createPoints(
        data: List<String>,
        pattern: HormonePattern,
        curvePoints: List<CurvePoint>
    ): List<ExaminationPoint> =
        pointsAnalyzer.analyze(data, pattern, curvePoints, extractor)

    /**
     * This method should be call after [ResultCalculationAnalyzer.createCurve]
     * in other case [Counter]  will not be setup previously and method
     * will case an Exception.
     *
     * @param pattern instance of [HormonePattern]
     * @return instance of [BaseGraph]
     */
    private fun createGraph(pattern: HormonePattern): BaseGraph = with(counter) {
        val coordinates: List<Coordinates> = createCoordinates(pattern)
        val r: Double = correlation
        val zeroBindingPercent: Double = zeroBinding
        BaseGraph(coordinates, r, zeroBindingPercent)
    }

    private fun createCoordinates(pattern: HormonePattern): List<Coordinates> =
        (0 until defineLimit(pattern)).map { Coordinates(x = counter.dose[it], y = counter.realZero[it]) }

    private fun defineLimit(pattern: HormonePattern): Int = pattern.run {
        (points.size - controlPoints - defineStart(pattern) - 1)
    }

    private fun defineStart(pattern: HormonePattern): Int = pattern.run {
        (totals + Zeros + NSBs)
    }
}
