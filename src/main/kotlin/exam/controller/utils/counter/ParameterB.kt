@file:Suppress("SpellCheckingInspection")

package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms
import org.apache.commons.math3.util.Pair
import org.apache.commons.math3.util.Precision.round
import kotlin.math.pow

object ParameterB : Algorithms {
    /**
     * Excel version:
     * N19
     * var M25:M40 => logDose
     * var N25:N40 => logarithmRealZero
     *
     *
     * =(COUNT(M25:M40) * SUMPRODUCT(M25:M40;N25:N40) -SUM(M25:M40)*SUM(N25:N40))/(COUNT(M25:M40) * SUMSQ(M25:M40)-(SUM(M25:M40))^2)
     */
    fun count(
        realZero: List<Double>,
        doseList: List<Double>
    ): Double = runCatching { round(createVariables(realZero, doseList), 4) }
        .onSuccess { println("Regression Parameter B finished with:${Ansi.COLOR.GREEN} $it ${Ansi.RESET}") }
        .onFailure { println("Regression Parameter B failed with: ${it.message}") }
        .getOrElse { 0.0 }

    private fun getDoseMultiplyBySumsq(countDose: Double, sumsqSecondFactor: Double): Double =
        (countDose * sumsqSecondFactor)

    private fun sumProduct(realZero: List<Double>, doseList: List<Double>): Double = realZero.indices
        .map { Pair(doseList[it], listRoundTo1(realZero)[it]) }
        .map { multiplyPairElements(it) }
        .sumOf { round(it, 3) }

    private fun multiplyDoseByRealZero(sumDose: Double, sumRealZero: Double): Double =
        sumDose * sumRealZero

    private fun multiplyDoseBySumProduct(countDose: Double, sumProduct: Double): Double =
        countDose * sumProduct

    @Suppress("UnnecessaryVariable")
    private fun createVariables(realZero: List<Double>, doseList: List<Double>): Double =
        run {
            val sumProduct = sumProduct(realZero, doseList)
            val countDose = getSize(doseList)
            val sumDose = sum(doseList)
            val sumRealZero = sum(realZero)
            val sumsqSecondFactor = sumsq(doseList)
            val dosePow2 = (sumDose).pow(2.0)
            val firstFactor =
                multiplyDoseBySumProduct(countDose, sumProduct) - multiplyDoseByRealZero(sumDose, sumRealZero)
            val secondFactor = getDoseMultiplyBySumsq(countDose, sumsqSecondFactor) - dosePow2
            val firstBySecond = getFirstBySecond(firstFactor, secondFactor)
            firstBySecond
        }

    private fun getFirstBySecond(firstFactor: Double, secondFactor: Double): Double = firstFactor / secondFactor
}
