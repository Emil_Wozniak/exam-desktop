package pl.ejdev.analyze.service.calculator

import pl.ejdev.analyze.calculator.Calculator
import pl.ejdev.analyze.model.data.DataSettings
import pl.ejdev.analyze.model.data.ProcessedData
import pl.ejdev.analyze.service.BaseSpec
import spock.lang.Narrative
import spock.lang.Shared
import spock.lang.Subject
import spock.lang.Title

@Title("Calculator specification")
@Narrative("""
Calculator takes examination settings and file lines,
then produces examination results. 
""")
class CalculatorSpec extends BaseSpec {

    @Subject
    private def calculator = Calculator.INSTANCE

    def "calculator returns calculated examination results samples #name are correct"() {
        given: "processed data with default settings and file lines"
        def data = new ProcessedData(lines, settings)

        when: "calculator counts data"
        def examinationResults = calculator.count(data)

        then: "result examination samples #name are equal to expected data"
        examinationResults.samples.collect { it[name] } == expected

        where:
        name      | expected
        'nr'      | (22..<48).collect()
        'cpm'     | lines.drop(settings.curve.size)
        'ngPerMl' | NG_PER_MLS
        'avg'     | AVGS
        'cv'      | CVS
        'sample'  | SAMPLES
    }

    private final static def SAMPLES = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13]

    private final static def CVS = [2.29, 2.29, 13.6, 13.6, 1.81, 1.81, 6.66, 6.66, 12.4, 12.4, 1.61, 1.61, 5.15, 5.15, 18.56, 18.56, 6.81, 6.81, 10.01, 10.01, 4.21, 4.21, 8.88, 8.88, 1.69, 1.69]

    private final static def AVGS = [11.13, 11.13, 17.17, 17.17, 10.48, 10.48, 6.16, 6.16, 3.71, 3.71, 1.86, 1.86, 2.24, 2.24, 4.85, 4.85, 9.04, 9.04, 9.79, 9.79, 11.77, 11.77, 5.18, 5.18, 3.55, 3.55]

    private final static List<Double> NG_PER_MLS = [
            11.38, 10.87,
            19.5, 14.83,
            10.67, 10.29,
            5.75, 6.57,
            4.17, 3.25,
            1.89, 1.83,
            2.35, 2.12,
            5.75, 3.95,
            8.42, 9.65,
            10.77, 8.81,
            12.26, 11.27,
            4.72, 5.64,
            3.61, 3.49
    ]
}