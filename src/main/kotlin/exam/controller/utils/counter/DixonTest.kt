package exam.controller.utils.counter

import kotlin.math.sqrt

/**
 * as Dixon's limit values starts from 3,
 * this value should be subtracted.
 */
const val DIXON_TABLE_START = 3

/**
 * Code based on [How to detect outliers in an ArrayList](https://stackoverflow.com/a/51782935)
 *
 * About [dixon test](https://en.wikipedia.org/wiki/Dixon%27s_Q_test)
 * @author [Valiyev](https://stackoverflow.com/users/6842675/valiyev)
 */
object DixonTest {
    /**
     * This table summarizes the limit values
     * of the two-tailed Dixon's Q test.
     * Represents Q90% values.
     */
    private val criticalValues = listOf(0.941, 0.765, 0.642, 0.56, 0.507, 0.468, 0.437)

    /**
     * Q90% value selected by number of values.
     */
    private var scaleOfElimination = 0.0
    private var mean = 0.0
    private var stdDev = 0.0

    /**
     * eliminate outliers with the help of Standard Deviation and Mean.
     *
     * @author [Valiyev](https://stackoverflow.com/users/6842675/valiyev)
     * Code based on [How to detect outliers in an ArrayList](https://stackoverflow.com/a/51782935)
     *
     * @param input examine list of values
     * @return list of not eliminate values
     */
    fun eliminateOutliers(input: List<Double>): List<Double> = input.let {
        val qValueSelector = it.size - DIXON_TABLE_START
        scaleOfElimination = criticalValues[qValueSelector]
        mean = getMean(it)
        stdDev = getStdDev(it)
        it.filter { point -> isOutOfBounds(point) }
    }

    /**
     * Code based on [How to detect outliers in an ArrayList](https://stackoverflow.com/a/51782935)
     * @author [Valiyev](https://stackoverflow.com/users/6842675/valiyev)
     * @param input examine values
     * @return mean
     */
    private fun getMean(input: List<Double>): Double = input.sum() / input.size

    /**
     * Code based on [How to detect outliers in an ArrayList](https://stackoverflow.com/a/51782935)
     * @author [Valiyev](https://stackoverflow.com/users/6842675/valiyev)
     * @param input examine values
     * @return variance
     */
    private fun getVariance(input: List<Double>): Double =
        getMean(input).let { mean ->
            input.map { (it - mean) * (it - mean) }.sum() / (input.size - 1)
        }

    /**
     * Code based on [How to detect outliers in an ArrayList](https://stackoverflow.com/a/51782935)
     * @author [Valiyev](https://stackoverflow.com/users/6842675/valiyev)
     * @param input examine values
     * @return standard dev
     */
    private fun getStdDev(input: List<Double>): Double =
        sqrt(getVariance(input))

    private fun isOutOfBounds(value: Double): Boolean =
        !(isLessThanLowerBound(value) || isGreaterThanUpperBound(value))


    private fun isGreaterThanUpperBound(value: Double): Boolean =
        value > mean + stdDev * scaleOfElimination

    private fun isLessThanLowerBound(value: Double): Boolean =
        value < mean - stdDev * scaleOfElimination

}
