package exam.controller.utils.counter

import exam.model.curve.CalibrationCurveValues
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint
import tornadofx.*

class BaseCounter : Controller(), Counter {
    override var dose: List<Double> = listOf()
    override var correlation: Double = 0.0
    override var zeroBinding: Double = 0.0
    override var realZero: List<Double> = listOf()
    override var calibrationCurveValues: CalibrationCurveValues = CalibrationCurveValues()
    override var standards: MutableList<Int> = mutableListOf()
    override var paramB: Double = 0.0
    override var paramA: Double = 0.0
    override var bindingPercentage: List<Double> = listOf()

    override fun countMeters(pattern: HormonePattern, curve: List<CurvePoint>): List<Double> {
        this.calibrationCurveValues = calibrationCurve(pattern, curve)
        this.standards = extractStandard(curve, this.calibrationCurveValues)
        this.bindingPercentage = countBinding(this.standards, this.calibrationCurveValues)
        this.realZero = countRealZero(this.bindingPercentage)
        this.zeroBinding = zeroBind(this.calibrationCurveValues)
        this.dose = countDose(this.calibrationCurveValues, pattern)
        this.paramB = countB(this.realZero, this.dose)
        this.paramA = countA(this.realZero, this.dose, this.paramB)
        this.correlation = findCorrelation(pattern, this.dose, this.realZero)
        return countReadMeter(this.standards, this.calibrationCurveValues, this.paramA, this.paramB)
    }

    /**
     * Standard storing CMP and Flag
     * tableC && tableG -> Control Curve CMP
     */
    private fun extractStandard(curve: List<CurvePoint>, dimensions: CalibrationCurveValues): MutableList<Int> = curve
        .drop(dimensions.standardStart)
        .take(dimensions.standardEnd)
        .map(CurvePoint::cpm)
        .toMutableList()

    private fun calibrationCurve(pattern: HormonePattern, curve: List<CurvePoint>): CalibrationCurveValues =
        CalibrationCurveCounter.calibrate(pattern, curve)

    override fun calculateDoseLogarithm(pattern: HormonePattern) = calibrationCurve(pattern, listOf())

    override fun countNGs(cpms: List<Double>): List<Double> =
        NGCalculator.calculate(cpms, this.calibrationCurveValues, this.paramA, this.paramB)

    private fun zeroBind(dimensions: CalibrationCurveValues): Double =
        ZeroBindingPercentage.count(dimensions)

    private fun findCorrelation(pattern: HormonePattern, dose: List<Double>, realZero: List<Double>): Double =
        Correlation.count(pattern, dose, realZero)

    private fun countB(realZero: List<Double>, doseList: List<Double>): Double =
        ParameterB.count(realZero, doseList)

    private fun countA(
        realZero: List<Double>,
        dose: List<Double>,
        parameterB: Double
    ): Double = ParameterA.count(realZero, dose, parameterB)

    private fun countDose(dimensions: CalibrationCurveValues, pattern: HormonePattern): List<Double> =
        Dose.count(dimensions, pattern)

    private fun countRealZero(bindingPercent: List<Double>): List<Double> =
        RealZero.count(bindingPercent)

    private fun countReadMeter(
        standards: List<Int>,
        dimensions: CalibrationCurveValues,
        parameterA: Double,
        parameterB: Double
    ): List<Double> = MeterRead.count(standards, dimensions, parameterA, parameterB)

    private fun countBinding(standards: List<Int>, dimensions: CalibrationCurveValues): List<Double> =
        BindingPercentage.count(standards, dimensions)
}
