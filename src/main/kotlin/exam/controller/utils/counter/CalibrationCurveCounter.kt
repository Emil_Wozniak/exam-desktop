package exam.controller.utils.counter

import exam.model.curve.CalibrationCurveValues
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint

object CalibrationCurveCounter {

    fun calibrate(hormone: HormonePattern, curve: List<CurvePoint>): CalibrationCurveValues =
        runCatching { CalibrationCurveValues(hormone, curve) }
            .onSuccess { println("T: ${it.total} | Zero: ${it.zero} | NSB: ${it.nsb} | N - O: ${it.binding} ") }
            .onFailure { println("Encourage error on ${it.message}") }
            .getOrElse { CalibrationCurveValues() }
}

