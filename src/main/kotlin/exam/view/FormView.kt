package exam.view

import exam.app.config.*
import exam.controller.TableController
import exam.model.hormone.HormonePattern
import exam.model.setting.FileSettings
import javafx.geometry.NodeOrientation.LEFT_TO_RIGHT
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.layout.VBox
import javafx.scene.text.FontWeight.BOLD
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

object FormView : View() {
    private val controller: TableController by inject()

    override val root: Parent = VBox().apply {

        nodeOrientation = LEFT_TO_RIGHT

    }


    /**
     * @param pattern values for [HormonePattern] implementation, for example: [DEFAULT_PATTERN]
     * @param startValue lowest value of [HormonePattern.points], for example: [DEFAULT_START_VALUE]
     * @see HormonePattern
     */
    private fun createSettings(pattern: String, startValue: String) {
        val isDefault = pattern == DEFAULT_PATTERN && startValue == DEFAULT_START_VALUE
        val sets = FileSettings(isDefault, pattern, startValue, DEFAULT_TARGET_POINT)
        println("Create new Settings: $sets")
        controller.setSettings(sets)
    }
}
