package pl.ejdev.analyze.service.curve

import pl.ejdev.analyze.model.curve.ControlCurve
import pl.ejdev.analyze.model.curve.CurveReadValues
import kotlin.math.ceil

internal object ControlCurveAnalyzer {
    fun analyze(curveReadValues: CurveReadValues): ControlCurve = curveReadValues.run {
        val totals: Double = ceil(this.totals.average())
        val zeros: Double = ceil(this.bg.average())
        val nsbs: Double = ceil(this.bo.average())
        val binding: Double = ceil(nsbs - zeros)
        ControlCurve(totals, zeros, nsbs, binding)
    }
}