package exam.controller.utils.analizer.extractor

fun metadata(data: List<String>): List<String> = data.drop(2).toList()
