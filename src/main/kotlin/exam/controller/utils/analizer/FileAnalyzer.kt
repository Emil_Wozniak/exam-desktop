package exam.controller.utils.analizer

import exam.controller.utils.converter.FileConverter
import exam.controller.utils.pattern.HormoneResolver
import exam.controller.utils.pattern.HormoneStrategy
import exam.controller.utils.reader.TxtFileReader
import exam.model.file.ExaminationFile
import exam.model.hormone.HormonePattern
import exam.model.results.ExaminationResults
import exam.model.setting.FileSettings
import tornadofx.*
import java.io.File

class FileAnalyzer : Controller(), AnalysisExecutor {

    private val fileConverter: FileConverter by inject()
    private val reader: TxtFileReader by inject()
    private val strategy: HormoneStrategy by inject<HormoneResolver>()
    private val resultsAnalyzer: ResultsAnalyzer by inject<ResultCalculationAnalyzer>()

    override fun analyze(uploadFile: File, settings: FileSettings): ExaminationResults {
        val examinationFile: ExaminationFile = fileConverter.convert(uploadFile, settings)
        val (file, _, patternTitle) = examinationFile
        println("Detected file: ${file.name}")
        val hormone = strategy.resolve(settings)
        val data = reader.read(examinationFile)
        return analyze(data, file.name, patternTitle, hormone)
    }

    override fun recalculate(results: ExaminationResults): ExaminationResults {
        val (title, patternTitle) = results
        val data: List<String> = recreateData(results)
        return analyze(data, title, patternTitle, results.pattern)
    }

    private fun analyze(
        data: List<String>,
        name: String,
        patternTitle: String,
        hormone: HormonePattern
    ): ExaminationResults =
        resultsAnalyzer.analyze(data, hormone, name, patternTitle)

    private fun recreateData(examinationResults: ExaminationResults): List<String> {
        val exam = examinationResults.controlCurve[0]
        val (_, identifier, pattern) = exam!!
        val control = examinationResults.controlCurve.withIndex()
            .map { (index, point) -> " \tUnk_$index\tR$index\t${point?.cpm ?: 0}\t0.0" }
        val points = examinationResults.resultPoints.withIndex()
            .map { (index, point) -> " \tUnk_$index\tR$index\t${point?.cpm ?: 0}\t0.0" }
        val results = mutableListOf(identifier, pattern)
        results += control
        results += points
        return results
    }
}
