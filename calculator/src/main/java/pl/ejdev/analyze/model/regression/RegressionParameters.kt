package pl.ejdev.analyze.model.regression

data class RegressionParameters(
    val n: Int,
    val b: Double,
    val a: Double,
    val r: Double,
    val graph: Graph
) {
    data class Graph(
        val points: List<Point>
    )

    data class Point(
        val x: Double,
        val y: Double,
        val pg: Double, // reading
        val nr: Int,
        val deltaPercentage: Int, // Δ%
        val ngTimesN: Double // ng * N
    )
}