package exam.controller.utils.reader

import exam.app.config.DEFAULT_TARGET_POINT
import exam.app.config.EMPTY
import exam.app.config.FILENAME_PART
import exam.app.config.PATTERN_PART
import exam.model.file.ExaminationFile
import tornadofx.Controller

/**
 * Is responsible for read Txt file content.
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class TxtFileReader : Controller(), FileReader {


    /**
     * Takes lines of target data from [ExaminationFile] and appends required metadata.
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun read(data: ExaminationFile): List<String> = with(data.contents.get()) {
        filter(String::isNotBlank).map { appendFileInfo(it, data) }.filter(String::isNotBlank)
    }

    /**
     * @param line   line of data matched to target start value
     * @param target characteristic point from which data will be received
     * @return if line contains target returns line if not empty string
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun isDataResult(line: String, target: String): String = if (line.startsWith(target)) line
    else EMPTY

    /**
     * @param data       defaults field determine behavior of method
     * @param streamData list of data from uploaded file
     * @return if defaults true list with uploaded filename and
     * information about what hormone pattern
     * have been used in examination the samples,
     * if false empty ArrayList
     * @see DEFAULT_TARGET_POINT
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun appendFileInfo(streamData: String, data: ExaminationFile): String = when {
        streamData.contains(PATTERN_PART) -> PATTERN_PART
        streamData.contains(FILENAME_PART) -> FILENAME_PART
        else -> streamData
    }.let {
        create(streamData, data, it)
    }

    private fun create(streamData: String, data: ExaminationFile, checker: String): String = when (checker) {
        PATTERN_PART -> getPatternFromData(streamData)
        FILENAME_PART -> getFileName(streamData)
        else -> isDataResult(streamData, data.target)
    }

    /**
     * @param pattern list of lines from the upload file;
     * @return the fifth line of streamData if starts from [PATTERN_PART]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun getPatternFromData(pattern: String): String = pattern.let {
        if (it.startsWith(PATTERN_PART)) {
            println("Not default pattern: $it")
            it.replace(PATTERN_PART, EMPTY)
        } else it
    }

    /**
     * @param startPath list of lines from the upload file;
     * @return the first line of streamData if starts from [PATTERN_PART]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun getFileName(startPath: String): String = startPath.let {
        if (it.startsWith(FILENAME_PART)) {
            println("Not default: $it")
            it.replace(FILENAME_PART, EMPTY)
        } else it
    }
}
