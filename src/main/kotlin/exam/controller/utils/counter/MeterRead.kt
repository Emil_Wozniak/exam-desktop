package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms
import exam.controller.utils.math.avoidNaNsOrInfinite
import exam.model.curve.CalibrationCurveValues
import exam.model.hormone.Point
import org.apache.commons.math3.util.Precision.round
import kotlin.math.log10

object MeterRead : Algorithms {

    /**
     * Excel version:
     * LOG() == decimal logarithm | Briggsian logarithm
     * var real_point = (G23 - $I$16)
     * var first_part = (G23 - $I$16) *100 / $J$18
     * var second_part = 100- ((G23 - $I$16) *100)
     * ################ [        first_part        ] / [          second_part            ] ###################
     * result =10^((LOG((G23 - $I$16) * 100 / $J$18 / (100 - (G23 - $I$16) * 100 / $J$18))) - $R$19) / $R$20).
     *
     * @return meter read in picograms for List of Point entities
     * @see Point
     */
    fun count(
        standards: List<Int>,
        dimensions: CalibrationCurveValues,
        parameterA: Double,
        parameterB: Double
    ): List<Double> = runCatching {
        standards
            .asSequence()
            .map { it.toDouble() }
            .map { getReadPoint(it, dimensions) }
            .map { log10(it) }
            .map { it - parameterA }
            .map { it / parameterB }
            .map { powerBy10(it) }
            .map { avoidNaNsOrInfinite(it) }
            .map { round(it, 2) }
            .toList()
    }
        .onSuccess { println("Reading Meter finished with:${Ansi.COLOR.GREEN} ${it.size} ${Ansi.RESET}points") }
        .onFailure { println("Reading Meter failed with:${it.cause}") }
        .getOrElse { listOf() }

    private fun getReadPoint(point: Double, calibrations: CalibrationCurveValues): Double {
        val nsbs = calibrations.nsb.toDouble()
        val binding = calibrations.binding.toDouble()
        val realPoint = point - nsbs
        // first part
        val realMultiplyTo100 = realPoint * 100.0
        val first = realMultiplyTo100 / binding
        // second part
        val realMinus100 = realPoint * 100
        val realMinus100ByBinding = realMinus100 / binding
        val second = 100 - realMinus100ByBinding
        return first / second
    }
}
