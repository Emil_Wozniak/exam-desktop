package pl.ejdev.analyze.calculator

import org.apache.commons.math3.util.Precision.*
import pl.ejdev.analyze.model.data.ProcessedData
import pl.ejdev.analyze.model.examination.ExaminationResults
import pl.ejdev.analyze.model.examination.SamplePoint
import pl.ejdev.analyze.model.regression.RegressionParameters
import pl.ejdev.analyze.model.standard.StandardGenerator
import pl.ejdev.analyze.model.standard.StandardPoint
import pl.ejdev.analyze.service.curve.ControlCurveAnalyzer
import pl.ejdev.analyze.service.regression.RegressionAnalyzer
import pl.ejdev.analyze.service.standard.StandardAnalyzer
import pl.ejdev.analyze.utils.copyEach
import pl.ejdev.analyze.utils.stdDevDouble
import kotlin.math.log10
import kotlin.math.pow

interface ExaminationResultCalculator {
    fun count(data: ProcessedData): ExaminationResults
}

internal object Calculator : ExaminationResultCalculator {
    override fun count(data: ProcessedData): ExaminationResults {
        val (_, zero, nsbs, binding) = ControlCurveAnalyzer.analyze(data.createCurveReadValues())
        val cortizol = StandardGenerator.generateCortizol()
        val standardPoints: List<StandardPoint> = StandardAnalyzer.analyze(cortizol, data.standardLines, zero, binding)
        val regressionParameters: RegressionParameters = RegressionAnalyzer.analyze(standardPoints, zero, nsbs)

        val results = data.samples
            .createPointResults(
                zero, binding, regressionParameters.a, regressionParameters.b,
                data.settings.repeats, data.settings.curve.size
            )

        return ExaminationResults(
            samples = results,
            standard = standardPoints,
            regressionParameters = regressionParameters
        )
    }

    private fun List<Int>.createPointResults(
        zero: Double,
        binding: Double,
        a: Double,
        b: Double,
        repeats: Int,
        curveSize: Int
    ): List<SamplePoint> {
        val ngPerMls = map { ngPerMl(it, zero, binding, a, b) }
        val cvAndAvgs = ngPerMls.countCvAndAvg(repeats)
        val samples = (0 until size / 2).toList().copyEach(repeats)
        return indices.map {
            SamplePoint(
                nr = curveSize + it + 1,
                cpm = this[it],
                ngPerMl = ngPerMls[it],
                cv = cvAndAvgs[it].first,
                avg = cvAndAvgs[it].second,
                sample = samples[it] + 1
            )
        }

    }

    /**
     * ```
     * =10^(
     *  (
     *    LOG(
     *      (CPM-ZERO)
     *      *100
     *      /Bo-Bg
     *      / (100 - (CPM-ZERO) *100 / Bo-Bg)
     *    )
     *    -A
     * )
     * /B
     * )
     * ```
     */
    private fun ngPerMl(cpm: Int, zero: Double, binding: Double, a: Double, b: Double): Double {
        val log = log10((cpm - zero) * 100 / binding / (100 - (cpm - zero) * 100 / binding))
        val withoutRegression = (log - a) / b
        return round(10.0.pow(withoutRegression), 2)
    }

    private fun List<Double>.countCvAndAvg(repeats: Int): List<Pair<Double, Double>> =
        this
            .withIndex()
            .groupBy { it.index / 2 }
            .map { it.value.map(IndexedValue<Double>::value) }
            .map {
                val average = it.average()
                val cv = 100 * it.stdDevDouble() / average
                round(cv, 2) to round(average, 2)
            }
            .copyEach(repeats = repeats)
}