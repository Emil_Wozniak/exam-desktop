package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms
import exam.controller.utils.math.avoidNaNsOrInfinite
import org.apache.commons.math3.util.Precision.round
import kotlin.math.log10

object RealZero : Algorithms {
    /**
     * Excel version:
     *
     *
     * Table J = LOG(H23/(100-H23))
     */
    fun count(bindingPercent: List<Double>): List<Double> =
        runCatching {
            bindingPercent.indices
                .map { processBindingPercent(it, bindingPercent) }
                .map { log10(it) }
                .map { round(it, 3) }
                .map { avoidNaNsOrInfinite(it) }
                .reversed()
        }
            .onSuccess { println("Logarithm Real Zero${Ansi.COLOR.GREEN} $it ${Ansi.RESET}") }
            .onFailure { println("Logarithm Real Zero failed with:${it.message}") }
            .getOrElse { emptyList() }

    private fun processBindingPercent(index: Int, bindingPercent: List<Double>): Double {
        val point = bindingPercent[index]
        val subtract = 100.0 - point
        return point / subtract
    }
}

