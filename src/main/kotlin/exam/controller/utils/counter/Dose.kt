package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms
import exam.controller.utils.math.avoidNaNsOrInfinite
import exam.model.curve.CalibrationCurveValues
import exam.model.hormone.HormonePattern
import org.apache.commons.math3.util.Precision.round
import kotlin.math.log10

object Dose : Algorithms {

    /**
     * take double array which contains a standardized pattern and
     * performs a logarithmic function for each element of the array
     */
    fun count(
        dimensions: CalibrationCurveValues,
        pattern: HormonePattern
    ): List<Double> = runCatching {
        pattern
            .points
            .asSequence()
            .drop(dimensions.standardStart)
            .take(dimensions.standardEnd)
            .map { it.value }
            .map { log10(it) }
            .map { round(it, 3) }
            .map { round(it, 2) }
            .map { avoidNaNsOrInfinite(it) }
            .toList()
    }
        .onSuccess { println("Logarithm Dose finished with:${Ansi.COLOR.GREEN} $it ${Ansi.RESET}") }
        .onFailure { println("Logarithm Dose failed with:${it.cause}") }
        .getOrElse { emptyList() }
}


