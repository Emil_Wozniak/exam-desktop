package exam.app.config

class Ansi {

    companion object AnsiCode {
        const val RESET = "\u001B[0m"
        const val BLACK = "\u001B[30m"
        const val RED = "\u001B[31m"
        const val GREEN = "\u001B[32m"
        const val YELLOW = "\u001B[33m"
        const val BLUE = "\u001B[34m"
        const val PURPLE = "\u001B[35m"
        const val CYAN = "\u001B[36m"
        const val WHITE = "\u001B[37m"
        const val BGD_BLACK = "\u001B[40m"
        const val BGD_RED = "\u001B[41m"
        const val BGD_GREEN = "\u001B[42m"
        const val BGD_YELLOW = "\u001B[43m"
        const val BGD_BLUE = "\u001B[44m"
        const val BGD_MAGENTA = "\u001B[45m"
        const val BGD_CYAN = "\u001B[46m"
        const val BGD_WHITE = "\u001B[47m"
        const val SANE = "\u001B[0m"
        const val HIGH_INTENSITY = "\u001B[1m"
        const val LOW_INTENSITY = "\u001B[2m"
        const val ITALIC = "\u001B[3m"
        const val UNDERLINE = "\u001B[4m"
        const val BLINK = "\u001B[5m"
        const val RAPID_BLINK = "\u001B[6m"
        const val REVERSE_VIDEO = "\u001B[7m"
        const val INVISIBLE_TEXT = "\u001B[8m"

    }

    /**
     * Ansi text colors
     */
    object COLOR {
        // ANSI escape code
        const val BLACK = exam.app.config.Ansi.AnsiCode.BLACK
        const val RED = exam.app.config.Ansi.AnsiCode.RED
        const val GREEN = exam.app.config.Ansi.AnsiCode.GREEN
        const val YELLOW = exam.app.config.Ansi.AnsiCode.YELLOW
        const val BLUE = exam.app.config.Ansi.AnsiCode.BLUE
        const val PURPLE = exam.app.config.Ansi.AnsiCode.PURPLE
        const val CYAN = exam.app.config.Ansi.AnsiCode.CYAN
        const val WHITE = exam.app.config.Ansi.AnsiCode.WHITE
    }

    /**
     * Ansi background colors
     */
    object BACKGROUND {
        const val BLACK = exam.app.config.Ansi.AnsiCode.BGD_BLACK
        const val RED = exam.app.config.Ansi.AnsiCode.BGD_RED
        const val GREEN = exam.app.config.Ansi.AnsiCode.BGD_GREEN
        const val YELLOW = exam.app.config.Ansi.AnsiCode.BGD_YELLOW
        const val BLUE = exam.app.config.Ansi.AnsiCode.BGD_BLUE
        const val MAGENTA = exam.app.config.Ansi.AnsiCode.BGD_MAGENTA
        const val CYAN = exam.app.config.Ansi.AnsiCode.BGD_CYAN
        const val WHITE = exam.app.config.Ansi.AnsiCode.BGD_WHITE
    }

    /**
     * Ansi text format props
     */
    object TEXT {
        const val SANE = exam.app.config.Ansi.AnsiCode.SANE
        const val HIGH_INTENSITY = exam.app.config.Ansi.AnsiCode.HIGH_INTENSITY
        const val LOW_INTENSITY = exam.app.config.Ansi.AnsiCode.LOW_INTENSITY
        const val ITALIC = exam.app.config.Ansi.AnsiCode.ITALIC
        const val UNDERLINE = exam.app.config.Ansi.AnsiCode.UNDERLINE
        const val BLINK = exam.app.config.Ansi.AnsiCode.BLINK
        const val RAPID_BLINK = exam.app.config.Ansi.AnsiCode.RAPID_BLINK
        const val REVERSE_VIDEO = exam.app.config.Ansi.AnsiCode.REVERSE_VIDEO
        const val INVISIBLE_TEXT = exam.app.config.Ansi.AnsiCode.INVISIBLE_TEXT
    }
}
