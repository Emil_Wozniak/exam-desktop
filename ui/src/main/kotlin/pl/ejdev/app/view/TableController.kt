package pl.ejdev.app.view

import exam.view.components.material.mdColumn
import exam.view.components.material.mdReadonlyColumn
import exam.view.components.material.mdTable
import javafx.scene.Parent
import javafx.scene.control.TableCell
import javafx.scene.control.TableView
import javafx.scene.layout.FlowPane
import javafx.scene.paint.Color
import org.kordamp.ikonli.materialdesign2.MaterialDesignA
import org.kordamp.ikonli.materialdesign2.MaterialDesignP
import pl.ejdev.app.controller.function.ResultPropertyController
import tornadofx.*

object TableController : View() {
    private val results: ResultPropertyController by inject()
    override val root: Parent = FlowPane().apply {
        hgap = 1.0
        vgap = 1.0
        with(exam.view.ScreenSize) {
            prefWidth = exam.view.ScreenSize.width * exam.view.sizeMod //1200.0
            prefWrapLength = exam.view.ScreenSize.height * exam.view.sizeMod // 1200.0
        }
        vbox {
            id = "curveVBox"
            spacing = 2.0
            label(exam.app.config.CALIBRATION_CURVE) {
                isCenterShape = true
                paddingAll = 8.0
                addClass("h2")
            }
            mdTable(results.curve) {
                id = "curveTable"
                isEditable = true
                mdColumn(exam.app.config.NR, exam.model.table.Curve::probeNumber) {
                    cellFormat {
                        text = "${it + 1}"
                        mute()
                    }
                }
                mdColumn(exam.app.config.POSITION, exam.model.table.Curve::position) {
                    cellFormat {
                        text = it
                        style { textFill = Color.DARKGRAY.darker() }
                        tooltip = tooltip {
                            text = when (it) {
                                "T" -> "Total"
                                "NSB" -> "Non-Specific binding (NSB)"
                                else -> it
                            }
                        }
                    }
                }
                mdColumn(exam.app.config.MEASUREMENT, exam.model.table.Curve::meterRead) {
                    cellFormat {
                        text = "$it"
                        style { textFill = Color.DARKGRAY.darker() }
                    }
                }
                mdColumn(exam.app.config.FLAG, exam.model.table.Curve::flagged) {
                    cellFormat { flagged ->
                        tooltip = tooltip {
                            text = when (flagged) {
                                true -> "Failed"
                                else -> "Succeed"
                            }
                        }
                        graphic = exam.view.components.material.materialDesignIcon(
                            icon = if (!flagged) MaterialDesignA.ALERT_CIRCLE_OUTLINE else MaterialDesignP.PLUS_CIRCLE_OUTLINE,
                            color = if (!flagged) Color.GREEN else Color.RED
                        )
                        isCenterShape = true
                        text = ""
                    }
                }
                mdColumn(exam.app.config.CPM, exam.model.table.Curve::cpm) { makeEditable() }

                enableCellEditing()
                regainFocusAfterEdit()
                onEditCommit { results.update(it) }
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                prefWidth = exam.view.ScreenSize.width * (exam.view.sizeMod + 0.2)
            }
        }
        vbox {
            id = "curveTable"
            spacing = 2.0
            label(exam.app.config.POINT_RESULTS) {
                isCenterShape = true
                paddingAll = 8.0
                addClass("h2")
            }
            mdTable(results.points) {
                id = "points-table"
                mdReadonlyColumn(exam.app.config.NR, exam.model.table.Point::probeNumber)
                mdReadonlyColumn(exam.app.config.NGS, exam.model.table.Point::ng)
                mdReadonlyColumn(exam.app.config.AVG, exam.model.table.Point::average)
                mdReadonlyColumn(exam.app.config.FLAG, exam.model.table.Point::flagged).cellFormat { flagged ->
                    graphic = exam.view.components.material.materialDesignIcon(
                        icon = if (!flagged) MaterialDesignA.ALERT_CIRCLE_OUTLINE else MaterialDesignP.PLUS_CIRCLE_OUTLINE,
                        color = if (!flagged) Color.GREEN else Color.RED
                    )
                    isCenterShape = true
                    text = ""
                }
                mdReadonlyColumn(exam.app.config.CPM, exam.model.table.Point::cpm)
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                prefWidth = exam.view.ScreenSize.width * (exam.view.sizeMod + 0.2)
            }
        }
    }

    private fun TableCell<exam.model.table.Curve, Int>.mute() {
        style { textFill = Color.DARKGRAY.darker() }
    }
}