package exam.controller.utils.pattern

import exam.controller.utils.analizer.FileAnalyzer
import exam.controller.utils.analizer.ResultsAnalyzer
import exam.model.hormone.CortisolPattern
import exam.model.hormone.CustomPattern
import exam.model.hormone.HormonePattern
import exam.model.setting.FileSettings

/**
 * Provides methods to resolve [ResultsAnalyzer]
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface HormoneStrategy {

    /**
     * @param settings object received from ExaminationService
     * @return instance of [ResultsAnalyzer]
     * @see FileAnalyzer
     */
    fun resolve(settings: FileSettings): HormonePattern

    /**
     * Takes [FileSettings.defaults] value, if it's true then returns value.
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun findBy(settings: FileSettings): HormonePattern {
        return if (settings.defaults) CortisolPattern()
        else isCustomPattern(settings)
    }

    fun isCustomPattern(settings: FileSettings): HormonePattern {
        val (_, pattern, startValue) = settings
        return CustomPattern(pattern, startValue.toDouble())
    }

}