package exam.controller.utils.pattern

import exam.model.hormone.HormonePattern
import exam.model.setting.FileSettings
import tornadofx.*

/**
 * Provides methods to resolve used type of [HormonePattern]
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
open class HormoneResolver : Controller(), HormoneStrategy {
    override fun resolve(settings: FileSettings): HormonePattern = findBy(settings)
}