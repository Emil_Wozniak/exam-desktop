package pl.ejdev.analyze.service.standard

import pl.ejdev.analyze.service.BaseSpec
import spock.lang.Subject
import spock.lang.Title

@Title("Standard analyzer specification")
class StandardAnalyzerSpec extends BaseSpec {

    @Subject
    private def analyzer = StandardAnalyzer.INSTANCE

    def "standard #label matches expected value"() {
        when: "analyzer analyze standard points"
        def standard = analyzer.analyze(STANDARD_POINTS, cpms, CURVE_ZERO_VALUE, BINDING)

        then: "standard #label points are equal to expected"
        standard.collect { it[name] } == expected

        where:
        name                | expected          | excel
        'cpm'               | cpms              | cpms
        'bindingPercent'    | bindingPercentage | bindingPercentage
        'logDose'           | logDose           | logDose
        'logarithmRealZero' | logarithmRealZero | EXCEL_LOGARITHM_REAL_ZERO
        label = labeled name

    }

    private final static def EXCEL_LOGARITHM_REAL_ZERO = [0.95, 0.74, 0.65, 0.49, 0.34, 0.31, -0.08, 0.02, -0.34, -0.30, -0.71, -0.62, -0.98, -1.10]
}