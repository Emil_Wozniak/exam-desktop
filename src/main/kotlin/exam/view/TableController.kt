package exam.view

import exam.app.config.*
import exam.controller.function.ResultPropertyController
import exam.model.table.Curve
import exam.model.table.Point
import exam.view.components.material.materialDesignIcon
import exam.view.components.material.mdColumn
import exam.view.components.material.mdReadonlyColumn
import exam.view.components.material.mdTable
import javafx.scene.Parent
import javafx.scene.control.TableCell
import javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY
import javafx.scene.layout.FlowPane
import javafx.scene.paint.Color
import javafx.scene.paint.Color.DARKGRAY
import org.kordamp.ikonli.materialdesign2.MaterialDesignA.ALERT_CIRCLE_OUTLINE
import org.kordamp.ikonli.materialdesign2.MaterialDesignP.PLUS_CIRCLE_OUTLINE
import tornadofx.*

object TableController : View() {
    private val results: ResultPropertyController by inject()
    override val root: Parent = FlowPane().apply {
        hgap = 1.0
        vgap = 1.0
        with(ScreenSize) {
            prefWidth = width * sizeMod //1200.0
            prefWrapLength = height * sizeMod // 1200.0
        }
        vbox {
            id = "curveVBox"
            spacing = 2.0
            label(CALIBRATION_CURVE) {
                isCenterShape = true
                paddingAll = 8.0
                addClass("h2")
            }
            mdTable(results.curve) {
                id = "curveTable"
                isEditable = true
                mdColumn(NR, Curve::probeNumber) {
                    cellFormat {
                        text = "${it + 1}"
                        mute()
                    }
                }
                mdColumn(POSITION, Curve::position) {
                    cellFormat {
                        text = it
                        style { textFill = DARKGRAY.darker() }
                        tooltip = tooltip {
                            text = when (it) {
                                "T" -> "Total"
                                "NSB" -> "Non-Specific binding (NSB)"
                                else -> it
                            }
                        }
                    }
                }
                mdColumn(MEASUREMENT, Curve::meterRead) {
                    cellFormat {
                        text = "$it"
                        style { textFill = DARKGRAY.darker() }
                    }
                }
                mdColumn(FLAG, Curve::flagged) {
                    cellFormat { flagged ->
                        tooltip = tooltip {
                            text = when (flagged) {
                                true -> "Failed"
                                else -> "Succeed"
                            }
                        }
                        graphic = materialDesignIcon(
                            icon = if (!flagged)  ALERT_CIRCLE_OUTLINE else PLUS_CIRCLE_OUTLINE,
                            color = if (!flagged) Color.GREEN else Color.RED
                        )
                        isCenterShape = true
                        text = ""
                    }
                }
                mdColumn(CPM, Curve::cpm) { makeEditable() }

                enableCellEditing()
                regainFocusAfterEdit()
                onEditCommit { results.update(it) }
                columnResizePolicy = CONSTRAINED_RESIZE_POLICY
                prefWidth = ScreenSize.width * (sizeMod + 0.2)
            }
        }
        vbox {
            id = "curveTable"
            spacing = 2.0
            label(POINT_RESULTS) {
                isCenterShape = true
                paddingAll = 8.0
                addClass("h2")
            }
            mdTable(results.points) {
                id = "points-table"
                mdReadonlyColumn(NR, Point::probeNumber)
                mdReadonlyColumn(NGS, Point::ng)
                mdReadonlyColumn(AVG, Point::average)
                mdReadonlyColumn(FLAG, Point::flagged).cellFormat { flagged ->
                    graphic = materialDesignIcon(
                        icon = if (!flagged) ALERT_CIRCLE_OUTLINE else PLUS_CIRCLE_OUTLINE ,
                        color = if (!flagged) Color.GREEN else Color.RED
                    )
                    isCenterShape = true
                    text = ""
                }
                mdReadonlyColumn(CPM, Point::cpm)
                columnResizePolicy = CONSTRAINED_RESIZE_POLICY
                prefWidth = ScreenSize.width * (sizeMod + 0.2)
            }
        }
    }

    private fun TableCell<Curve, Int>.mute() {
        style { textFill = DARKGRAY.darker() }
    }
}

private const val sizeMod = 0.4