package exam.controller.utils.math

/**
 * @param point      value that will be checked
 * @return while point value is NaN or Infinite point value will be changed to 0.0, in other way returns original value
 * @author emil.wozniak.591986@gmail.com
 * @since 22.01.2020
 */
fun avoidNaNsOrInfinite(point: Double): Double =
    if (point.isNaN() || point.isInfinite()) 0.0
    else point