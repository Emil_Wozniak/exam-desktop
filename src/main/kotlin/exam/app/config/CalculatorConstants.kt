@file:Suppress("SpellCheckingInspection")
package exam.app.config

const val TO_PERCENT = 100.0

const val EMPTY = ""
const val PATTERN_PART = "Name: COPY_OF_H-3_"
const val COLUMN_SPLICER = "\t"
const val ROW_SEPARATOR = ";"
const val NEGATIVE_ONE = "-1"
const val DEFAULT_TARGET_POINT = " \tUnk"
const val DEFAULT_PATTERN = "2;3;3;7;2;0"
const val DEFAULT_START_VALUE = "1.25"
const val FILENAME_PART = "C:\\mbw\\results\\"

const val CORTISOL_STANDARD = "Cortisol 1.25 "

