package exam.model.file

import cyclops.control.Eval
import exam.app.config.Ansi
import java.io.File
import java.util.function.Supplier

/**
 * Container for storing uploaded file metadata.
 */
data class ExaminationFile(
    val file: File,
    val target: String,
    val patternTitle: String,
    val defaults: Boolean? = false
) {


    /**
     * Stores content of uploaded file.
     */
    val contents: Supplier<List<String>> = Eval.later { loadContents() }

    private fun loadContents(): List<String> = runCatching { readLines().toList() }
        .onFailure { println(it.message) }
        .getOrElse { listOf() }

    /**
     * @return uploaded file lines on success or empty array list on failed.
     */
    private fun readLines(): MutableList<String> =
        runCatching {
            file
                .readLines()
                .toMutableList()
        }
            .onSuccess { println("Finished with: ${Ansi.COLOR.GREEN + " ${it.size} " + Ansi.RESET} lines") }
            .onFailure { println(it.message) }
            .getOrElse { mutableListOf() }
}
