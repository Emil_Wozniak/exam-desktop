package exam.controller.utils.analizer

import exam.model.hormone.HormonePattern
import exam.model.results.ExaminationResults

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface ResultsAnalyzer {

    fun analyze(
        data: List<String>,
        pattern: HormonePattern,
        name: String,
        patternTitle: String
    ): ExaminationResults

}
