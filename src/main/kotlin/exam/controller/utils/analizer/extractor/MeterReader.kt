package exam.controller.utils.analizer.extractor

import exam.controller.utils.counter.Counter
import exam.model.curve.CurveDimensions
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint

fun assignMeterToCurve(
    controlCurve: List<CurvePoint>,
    counter: Counter,
    dimensions: CurveDimensions,
    pattern: HormonePattern
): List<CurvePoint> {
    val meterReads: List<Double> = getMeterRead(pattern, controlCurve, counter)
    return controlCurve.indices.map { isPointInScope(dimensions.all, it, controlCurve, meterReads) }
}

/**
 * performs [CurvePoint.ng] on each element of {@param controlCurves}
 */
private fun getMeterRead(pattern: HormonePattern, curve: List<CurvePoint>, counter: Counter): List<Double> =
    counter.countMeters(pattern, curve)

/**
 * gets [CurvePoint] as point from [.controlCurve]
 * and if value of {@param i} is greater of equals than
 * {@param from} it will assign [.meterReads].
 */
private fun isPointInScope(
    from: Int, i: Int,
    controlCurve: List<CurvePoint>,
    meterReads: List<Double>
): CurvePoint {
    val point: CurvePoint = controlCurve[i]
    if (i >= from) point.meterRead = (meterReads[i - from])
    return point
}

