package exam.controller.utils.counter

object SpreadCounter {

    private const val DIXON_TEST_BORDER_VALUE = 10

    /**
     * if cpms can be tested, it will perform dixon test [.dixonTest].
     *
     * @param cpms examine values from upload file.
     * @return when pass [.isTestable] it will use [.dixonTest]
     * in another case [.skipTest]
     */
    fun isSpread(cpms: List<Int>): List<Boolean> =
        runCatching { if (isTestable(cpms)) dixonTest(cpms) else skipTest(cpms) }
            .onFailure { println("SpreadCounter failed with cause: ${it.cause}") }
            .getOrElse { skipTest(cpms) }

    private fun isTestable(cpms: List<Int>): Boolean =
        cpms.size in 3 until DIXON_TEST_BORDER_VALUE

    /**
     * @param cpms values to check
     * @return false values for each element of CPMs list,
     * if and only if CPMs can't be checked by [.dixonTest]
     * @see SpreadCounter.dixonTest
     */
    private fun skipTest(cpms: List<Int>): List<Boolean> = cpms.map { false }

    private fun dixonTest(values: List<Int>): List<Boolean> {
        val examinePoints = values.map { it.toDouble() }
        val eliminateOutliers: List<Double> = DixonTest.eliminateOutliers(examinePoints)
        return examinePoints.getResult(eliminateOutliers)
    }

    private fun List<Double>.getResult(outliers: List<Double>): List<Boolean> =
        this.map { !outliers.contains(it) }

}
