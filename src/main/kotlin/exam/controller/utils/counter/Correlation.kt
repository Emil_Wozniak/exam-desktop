package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.model.hormone.HormonePattern
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation
import org.apache.commons.math3.util.Precision.round
import kotlin.math.pow

object Correlation {
    fun count(
        pattern: HormonePattern,
        dose: List<Double>,
        realZero: List<Double>,
    ): Double {
        val controlCurveSize = getControlCurveSize(pattern)
        return runCatching { createCorrelationArrays(dose, realZero, controlCurveSize) }
            .map { PearsonsCorrelation().correlation(it[0], it[1]) }
            .map { (it).pow(2.0) }
            .map { round(it, 6) }
            .onSuccess { println("Correlation finished with:${Ansi.COLOR.GREEN} $it ${Ansi.RESET}") }
            .onFailure { println("Correlation failed with:${it.cause}") }
            .getOrElse { 0.0 }
    }

    private fun getControlCurveSize(pattern: HormonePattern): Int =
        pattern.run {
            points.size - totals - NSBs - Zeros - controlPoints
        }

    private fun createCorrelationArrays(dose: List<Double>, realZero: List<Double>, size: Int): Array<DoubleArray> =
        Array(2) { DoubleArray(size) }
            .also {
                (0 until size)
                    .forEach { point ->
                        it[0][point] = dose[point]
                        it[1][point] = realZero[point]
                    }
            }
}
