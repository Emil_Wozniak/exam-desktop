package exam.app

import javafx.geometry.Pos
import javafx.scene.paint.Color.*
import javafx.scene.text.FontWeight.BOLD
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val column by cssclass()
        val tableLabel by cssclass()
        val lineChart by cssclass()
    }

    init {
        lineChart {
            stroke = TRANSPARENT
        }
        accordion {
            title {
                focusColor = TRANSPARENT
            }
            titledPane {
                title {
                    borderColor += box(DARKGRAY)
                    backgroundColor += TRANSPARENT
                    alignment = Pos.CENTER
                    arrowButton {
                        prefHeight = 12.px
                        prefWidth = 12.px
                    }
                    fontSize = 15.px
                }
            }
        }
        label and heading {
            textAlignment = CENTER
            padding = box(10.px)
            fontSize = 20.px
            fontWeight = BOLD
        }
        column {
            cellWidth = 50.percent
        }
        tableRowCell {
            wrapText = true
        }
        tableRowCell and hover {
            backgroundColor += LIGHTBLUE
            textFill = DARKGRAY
        }

        tableLabel {
            fontSize = 14.px
            fontWeight = BOLD
            textAlignment = CENTER
            padding = box(8.0.px)
            alignment = Pos.CENTER
        }
    }
}