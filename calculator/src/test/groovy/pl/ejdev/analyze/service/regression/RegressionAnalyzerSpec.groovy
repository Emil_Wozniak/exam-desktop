package pl.ejdev.analyze.service.regression

import pl.ejdev.analyze.model.data.DataSettings
import pl.ejdev.analyze.model.data.ProcessedData
import pl.ejdev.analyze.service.BaseSpec
import pl.ejdev.analyze.service.curve.ControlCurveAnalyzer
import pl.ejdev.analyze.service.standard.StandardAnalyzer
import spock.lang.Subject
import spock.lang.Title

@Title("Regression analyzer specification")
class RegressionAnalyzerSpec extends BaseSpec {

    @Subject
    private def analyzer = RegressionAnalyzer.INSTANCE

    def "regression param #name is correct"() {
        given: "processed data contains file lines and default settings"
        def processedData = new ProcessedData(lines, settings)

        and: "control curve analyzer creates control curve"
        def controlCurve = ControlCurveAnalyzer.INSTANCE.analyze(processedData.createCurveReadValues())

        and: "standard analyzer creates standard points"
        def standardPoints = StandardAnalyzer.INSTANCE.analyze(STANDARD_POINTS, cpms, CURVE_ZERO_VALUE, BINDING)

        when: "regression analyzer analyze standard points"
        def regressionParameters = analyzer.analyze(standardPoints, controlCurve.zeros, controlCurve.nsbs)

        then: "regression point #name are correct"
        regressionParameters[name] == expected

        if (expected != excel) excelMismatch { [(expected): excel] }

        where:
        name | expected            | excel
        'r'  | -0.9934             | -0.9934
        'b'  | -1.0440476190476182 | -1.04030283575261
        'a'  | 0.9969047619047611  | 0.994582780885827
    }
}