package exam.model.curve

import exam.controller.utils.counter.CalibrationCurveCounter
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint
import java.lang.Integer.sum

/**
 * Represents Calibration Curve standard values, it is instantiating by
 * CalibrationCurveCounter.
 * @property total average value of all highest points in the standard
 * @property nsb non-specific binding average value of antibody-binding response
 * @property zero average value of all lowest points in a standard
 * @property binding rest of subtraction from zero and nsb
 * @property standardStart sum of number of the totals, zeros and nsbs
 * @property standardEnd number of all standard points subtracts control points
 * @constructor default instance of a calibration curve if
 * CalibrationCurveCounter failed.
 *
 * @see CalibrationCurveCounter.calibrate
 *
 * [wiki](https://en.wikipedia.org/wiki/Calibration_curve)
 */
data class CalibrationCurveValues(
    var total: Int = 0,
    var zero: Int = 0,
    var nsb: Int = 0,
    var binding: Int = 0,
    var standardStart: Int = 0,
    var standardEnd: Int = 0
) {

    constructor(hormone: HormonePattern, curve: List<CurvePoint>) : this() {
        with(hormone) {
            total = calculateTotals(curve.range(0, totals))
            zero = calculatePoint(curve.range(NSBs + totals, NSBs + totals + Zeros))
            nsb = calculatePoint(curve.range(totals, NSBs + totals))
            binding = zero - nsb
            standardStart = standardStart()
            standardEnd = standardEnd()
        }
    }

    /**
     * @param totals list of [CurvePoint.cpm]
     * @return sum of each TOTALS values divided by totalsSize
     */
    private fun calculateTotals(totals: List<CurvePoint>): Int =
        totals.map(CurvePoint::cpm).sum() / totals.size

    /**
     * controlCurve sublist from [CalibrationCurveCounter.calibrate]
     * controlCurve, separated by [.forPoints] value calculated by removing flagged elements from
     * controlCurve, then converts to [CurvePoint.cpm] after that sum those values and divides by
     * number of remain elements
     */
    private fun calculatePoint(curve: List<CurvePoint>): Int {
        val flaggedPointCpm: Int = curve.getNotFlaggedPointCpm()
        val count: Int = curve.pointsCount()
        return flaggedPointCpm / count
    }

    private fun List<CurvePoint>.getNotFlaggedPointCpm(): Int = this
        .filter(::isNotFlagged)
        .map(CurvePoint::cpm)
        .sum()

    private fun List<CurvePoint>.pointsCount(): Int =
        this.count(::isNotFlagged)

    private fun isNotFlagged(point: CurvePoint): Boolean = !point.flagged!!

    private fun List<CurvePoint>.range(from: Int, to: Int): List<CurvePoint> =
        (from until to).map { this[it] }

    private fun HormonePattern.standardStart(): Int =
        (this.totals + this.Zeros + this.NSBs)

    private fun HormonePattern.standardEnd(): Int =
        (this.points.size - this.controlPoints)
}