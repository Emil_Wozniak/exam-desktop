package exam.model.hormone

import exam.app.config.ROW_SEPARATOR

private val regex = ROW_SEPARATOR.toRegex()

data class CustomPattern(
    override var totals: Int = 0,
    override var NSBs: Int = 0,
    override var Zeros: Int = 0,
    override var length: Int = 0,
    override var repeats: Int = 0,
    override var startValue: Double = 0.0,
    override var points: List<Point> = mutableListOf(),
    override var controlPoints: Int = 0
) : HormonePattern(
    totals,
    NSBs,
    Zeros,
    length,
    repeats,
    startValue,
    points,
    controlPoints
) {

    /**
     * @param pattern for example, "2;3;3;7;2;0"
     */
    constructor(pattern: String, startValue: Double) : this() {
        val patternValues = pattern.split(regex).map { it.toInt() }.toMutableList()
        this.totals = patternValues[0]
        this.NSBs = patternValues[1]
        this.Zeros = patternValues[2]
        this.length = patternValues[3]
        this.repeats = patternValues[4]
        this.startValue = startValue
        this.points = points(patternValues, startValue)
        this.controlPoints = patternValues[5]
    }
}