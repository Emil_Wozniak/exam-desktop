package exam.controller.utils.counter

import exam.app.config.Ansi
import exam.controller.utils.math.Algorithms

object ParameterA : Algorithms {

    /**
     * Excel version:
     * R19 == N20
     * var M25:M40 => logDose
     * var N25:N40 => logarithmRealZero
     * = sum(N25:N40) / count(M25:M40) - N19 => component.getRegressionParameterB() * sum(M25:M40) / count(M25:M40)
     */
    fun count(
        realZero: List<Double>,
        dose: List<Double>,
        parameterB: Double
    ): Double =
        runCatching { sumBySize(realZero, dose) - paramBTimesDose(parameterB, dose) }
            .onSuccess { println("Regression Parameter A finished with:${Ansi.COLOR.GREEN} $it ${Ansi.RESET}") }
            .onFailure { println("Regression Parameter A failed with: $it") }
            .getOrElse { 0.0 }

    private fun doseSumByDoseSize(dose: List<Double>): Double = this.sum(dose) / dose.size

    private fun sumBySize(realZero: List<Double>, dose: List<Double>): Double = sumRound1(realZero) / dose.size

    private fun paramBTimesDose(parameterB: Double, dose: List<Double>): Double = parameterB * doseSumByDoseSize(dose)

}
