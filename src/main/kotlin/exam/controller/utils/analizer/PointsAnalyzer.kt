package exam.controller.utils.analizer

import exam.app.config.DEFAULT_COLUMN
import exam.controller.utils.analizer.extractor.*
import exam.controller.utils.counter.BaseCounter
import exam.controller.utils.counter.Counter
import exam.controller.utils.counter.PointAverage
import exam.controller.utils.extractor.DataExtractor
import exam.model.curve.CurveDimensions
import exam.model.hormone.HormonePattern
import exam.model.results.CurvePoint
import exam.model.results.ExaminationPoint
import tornadofx.*

/**
 * Is responsible for analyze data and creates instances of ExaminationPoint.
 *
 * @see ResultsAnalyzer
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class PointsAnalyzer : Controller() {

    private val counter: Counter by inject<BaseCounter>()

    /**
     * @param pattern instance of [HormonePattern], which is provided from PatternFactory.
     * @param controlCurve instance containing List<CurvePoint>] is provided for calculation
     * @param withCurve false by default, while true it will perform create Curve Points
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun analyze(
        data: List<String>,
        pattern: HormonePattern,
        controlCurve: List<CurvePoint>,
        extractor: DataExtractor,
        columnNumber: Int? = null,
        withCurve: Boolean = false,
    ): List<ExaminationPoint> {
        val dimensions = dimension(pattern)
        recreateCurve(counter, dimensions, pattern, controlCurve, withCurve)
        val metadata = metadata(data)
        val filename = filename(data)
        val patternData = patternData(data)
        val cpms = cpms(extractor, metadata, columnNumber ?: DEFAULT_COLUMN)
        val length = cpms.size
        val flags = flags(cpms, dimensions, controlCurve)
        val ng = nanograms(counter, cpms)
        val avg = avg(ng)
        return points(
            pattern,
            length,
            filename,
            patternData,
            cpms,
            flags,
            ng,
            avg
        )
    }

    private fun recreateCurve(
        counter: Counter,
        dimensions: CurveDimensions,
        pattern: HormonePattern,
        controlCurve: List<CurvePoint>,
        withCurve: Boolean = false,
    ) {
        if (withCurve) assignMeterToCurve(
            controlCurve,
            counter,
            dimensions,
            pattern
        )
    }

    private fun avg(nanograms: List<Double>): List<Double> =
        PointAverage(nanograms).count()
}
