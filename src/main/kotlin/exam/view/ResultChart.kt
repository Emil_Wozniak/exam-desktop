package exam.view

import exam.app.Styles.Companion.lineChart
import exam.app.config.*
import exam.controller.function.ResultPropertyController
import javafx.scene.chart.NumberAxis
import javafx.scene.layout.HBox
import javafx.scene.paint.Color.TRANSPARENT
import tornadofx.*

object ResultChart : View("Chart") {
    private val results: ResultPropertyController by inject()

    override val root = HBox().apply {
        val allSize = ScreenSize.width * 0.3 //600.0
        prefWidth = allSize
        prefHeight = allSize
        this += linechart(CHART, NumberAxis(), NumberAxis()) {
            prefWidth = allSize
            prefHeight = allSize
            createSymbols = true
            layoutX = 100.0
            translateX = 1.0
            xAxis.tickLabelFill = TRANSPARENT
            addClass(lineChart)
            with(xAxis) {
                label = POINT
                animated = true
            }
            with(yAxis) {
                label = RESULT
                animated = true
            }
            series(SERIES_1, results.line1)
            series(SERIES_2, results.line2)
        }
    }
}
